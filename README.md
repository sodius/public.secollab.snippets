# SECollab Snippets

This repository holds resources for a developer to contribute new capabilities to the SECollab application.
Refer to the **SECollab Developer Guide** for information on how to contribute extensions and exploit the snippets of this repository.

See https://www.sodius.com/en/products/secollab

## Querying resources from SECollab server using a Java client

This reporistory contains snippets demonstrating how to use Java code to query resources stored on a SECollab server application.

Those snippets are located here: **/src/com.sodius.oslc.app.secollab.snippets/src/com/sodius/oslc/app/secollab/snippets/**

The Javadoc of each snippet details the expected Java VM argument for the snippet to run properly.

The first snippet to execute is **ListProjectsSnippet.java**. This snippet lists all projects available in the SECollab server. Such snippet output can then be used as argument of the next snippet, would is **ListDesignsSnippet**.


## Contributing a SECollab Publisher

A Publisher is what enables a user to extract content from an Authoring Tool 
and to project it as OSLC resources into a SECollab server.

SECollab comes with a certain number of publishers that handle content of major ALM tools.
If you have content in a specific tool or specific file format that you need to publish to SECollab, 
developing a new publisher plug-in allows to extend the SECollab capabilities.

The **src/com.sodius.mdw.clm.client.tool.sample** project contains source code to demonstrate a sample publisher.
It may be used as a basis to understand how to architect your own publisher.
It is notably referenced by the SECollab Developer Guide to get details on some tasks.

The **src/com.sodius.mdw.clm.client.tool.sample/data/sample.xml** file is an XML file that can be used as sample input for the publisher.
The XML file contains:

* A list of *specification* elements. Each specification is a set of *requirements* which constitute some statement of need. Requirements are organized as a tree.
* A list of *item* elements. An item is a resource (e.g. Component) that defines a part of the system.
* A list of *diagram* elements. A diagram is a visual representation of items and of their interactions. A .png file is associated to each diagram and is stored in the directory of the XML file.