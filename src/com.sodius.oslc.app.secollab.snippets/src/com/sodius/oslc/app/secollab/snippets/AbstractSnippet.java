package com.sodius.oslc.app.secollab.snippets;

import java.util.concurrent.Callable;

import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;

import com.sodius.oslc.client.ClientWebException;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.OslcClients;

/**
 * Base class for SECollab snippets.
 *
 * <p>
 * Requires VM arguments:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: path to the Sodius license file in your file system</li>
 * <li><code>secollab.user</code>: SECollab User ID</li>
 * <li><code>secollab.password</code>: SECollab password</li>
 * </ul>
 */
public abstract class AbstractSnippet implements Callable<Void> {

    @Override
    public Void call() throws Exception {
        try {
            // create client
            OslcClient client = OslcClients.basic(getSECollabCredentials()).create();

            call(client);
        } catch (ClientWebException e) {
            System.err.println("Failed to execute " + e.getRequest().getURI());
            System.err.println("Status " + e.getResponse().getStatusCode() + " - " + e.getResponse().getMessage());
            System.err.println("Entity: " + e.getResponse().getEntity(String.class));
        }

        return null;
    }

    final Credentials getSECollabCredentials() {
        return new UsernamePasswordCredentials(getRequiredProperty("secollab.user"), getRequiredProperty("secollab.password"));
    }

    static String getRequiredProperty(String name) {
        String value = System.getProperty(name);
        if ((value == null) || value.isEmpty()) {
            throw new IllegalArgumentException("Missing required VM argument: " + name);
        } else {
            return value;
        }
    }

    protected abstract void call(OslcClient client) throws Exception;
}
