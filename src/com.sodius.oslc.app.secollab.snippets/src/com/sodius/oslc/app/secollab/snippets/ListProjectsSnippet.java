package com.sodius.oslc.app.secollab.snippets;

import java.net.URI;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetRootServices;
import com.sodius.oslc.client.requests.GetServiceProviderCatalog;
import com.sodius.oslc.client.requests.ValidateConnection;
import com.sodius.oslc.core.model.RootServices;

/**
 * <p>
 * Validates the specified rootservices location.
 * It notably ensures the server is accessible, the provided credentials allows connecting and that the server is indeed a SECollab application.
 * It then reads the rootservices to determine the location of the ServiceProviderCatalog.
 * It finally loads the catalog to list the available projects.
 * </p>
 *
 * <p>
 * Required VM arguments:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: path to the Sodius license file in your file system</li>
 * <li><code>secollab.user</code>: SECollab User ID</li>
 * <li><code>secollab.password</code>: SECollab password</li>
 * <li><code>secollab.rootservices</code>: URL of the SECollab rootservices</li>
 * </ul>
 */
public class ListProjectsSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListProjectsSnippet().call();
    }

    @Override
    protected void call(OslcClient client) {

        // validate repository
        System.out.println("Validating connection to SECollab...");
        URI rootServicesLocation = URI.create(getRequiredProperty("secollab.rootservices"));
        IStatus status = new SECollabValidateConnection(client, rootServicesLocation).call();
        if (!status.isOK()) {
            System.out.println("Connection failure!");
            System.out.println(status.getMessage());
            return;
        }

        // read root services
        System.out.println("Reading Root Services...");
        RootServices rootServices = new GetRootServices<>(rootServicesLocation, RootServices.class).call();

        // read RM catalog
        // (The RootServices http://open-services.net/xmlns/rm/1.0/rmServiceProviders predicate)
        System.out.println("Reading Catalog...");
        ServiceProviderCatalog catalog = new GetServiceProviderCatalog(client, rootServices.getRmCatalog()).get();
        System.out.println("Catalog has " + catalog.getServiceProviders().length + " projects:");
        for (ServiceProvider provider : catalog.getServiceProviders()) {
            System.out.println("- " + provider.getTitle());
            System.out.println("        URL: " + provider.getAbout());
        }
    }

    /**
     * Validates the connection to a SECollab server.
     * The request notably ensures the remote application is indeed a SECollab instance.
     */
    private static class SECollabValidateConnection extends ValidateConnection<RootServices> {

        public SECollabValidateConnection(OslcClient client, URI rootServices) {
            super(client, rootServices, RootServices.class);
        }

        @Override
        protected IStatus call(RootServices rootServices) {
            return validatePublisher(rootServices, "http://sodius.com/application/secollab");
        }
    }
}
