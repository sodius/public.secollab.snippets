package com.sodius.oslc.app.secollab.model;

import java.net.URI;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.lyo.oslc4j.core.annotation.OslcDescription;
import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNamespace;
import org.eclipse.lyo.oslc4j.core.annotation.OslcOccurs;
import org.eclipse.lyo.oslc4j.core.annotation.OslcPropertyDefinition;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.annotation.OslcTitle;
import org.eclipse.lyo.oslc4j.core.annotation.OslcValueType;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import com.sodius.mdw.clm.common.model.Clm;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.OslcCore;

@OslcNamespace(Clm.NAMESPACE_URI)
@OslcName("Tool")
@OslcResourceShape(title = "CLM Tool Resource Shape", describes = Clm.NAMESPACE_URI + "Tool")
public class Tool extends AbstractResource {

    private String identifier;
    private String title;
    private final SortedSet<URI> domains = new TreeSet<>();

    @OslcDescription("A unique identifier for a resource. Assigned by the service provider when a resource is created. Not intended for end-user display.")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "identifier")
    @OslcTitle("Identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    @OslcDescription("Title of the resource")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "title")
    @OslcTitle("Title")
    @OslcValueType(ValueType.XMLLiteral)
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @OslcDescription("URIs of the OSLC domain specifications that may be implemented by referenced services")
    @OslcName("domain")
    @OslcPropertyDefinition(OslcCore.NAMESPACE_URI + "domain")
    @OslcTitle("Domains")
    public URI[] getDomains() {
        return domains.toArray(new URI[domains.size()]);
    }

    public void addDomain(final URI domain) {
        this.domains.add(domain);
    }

    public void setDomains(final URI[] domains) {
        this.domains.clear();
        if (domains != null) {
            this.domains.addAll(Arrays.asList(domains));
        }
    }
}
