package com.sodius.oslc.app.secollab.snippets;

import java.net.URI;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.Property;
import org.eclipse.lyo.oslc4j.core.model.ResourceShape;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import com.google.common.base.Strings;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResourceShape;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.am.model.ArchitectureResource;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;
import com.sodius.oslc.domain.rm.model.Requirement;

/**
 * <p>
 * Reads the specified SECollab resource and prints out its properties, discovering them by inspecting its resource shape.
 * </p>
 *
 * <p>
 * Required VM arguments:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: path to the Sodius license file in your file system</li>
 * <li><code>secollab.user</code>: SECollab User ID</li>
 * <li><code>secollab.password</code>: SECollab password</li>
 * <li><code>secollab.resource</code>: URL of a the SECollab resource</li>
 * <li><code>secollab.resource.type</code>: Type of a the SECollab resource (http://open-services.net/ns/rm#Requirement or
 * http://open-services.net/ns/am#Resource)</li>
 * </ul>
 *
 * @see ListDesignResourcesSnippet
 */
public class ReadResourceSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ReadResourceSnippet().call();
    }

    @Override
    protected void call(OslcClient client) throws Exception {

        // load requirement
        System.out.println("Reading resource...");
        URI requirementUri = URI.create(getRequiredProperty("secollab.resource"));
        URI configuration = OslcConfig.getContext(requirementUri);
        IExtendedResource resource = new GetResource<>(client, requirementUri, getResourceClass()).get();

        // load resource shape
        System.out.println("Reading resource type...");
        URI shapeUri = OslcConfig.addContext(getInstanceShape(resource), configuration);
        ResourceShape shape = new GetResourceShape(client, shapeUri).get();

        // display properties
        System.out.println("Resource properties:");
        List<Property> properties = getSortedProperties(shape);
        for (Property property : properties) {
            System.out.println();
            print(resource, property);
        }
    }

    private static Class<? extends IExtendedResource> getResourceClass() {
        String type = getRequiredProperty("secollab.resource.type");
        if (OslcRm.TYPE_REQUIREMENT.equals(type)) {
            return Requirement.class;
        } else {
            return ArchitectureResource.class;
        }
    }

    private static URI getInstanceShape(IExtendedResource resource) {
        return ResourceProperties.getURI(resource, OslcCore.PROPERTY_INSTANCE_SHAPE);
    }

    private void print(IExtendedResource resource, Property property) {

        // title
        System.out.println(property.getTitle() + " <" + property.getPropertyDefinition() + '>');

        // type
        System.out.println("  type:  " + getDisplayType(property));

        // value
        printValue(resource, property);
    }

    private void printValue(IExtendedResource resource, Property property) {

        // resource property?
        if (isResource(property)) {
            Collection<Link> links = ResourceProperties.getLinks(resource, property);

            // a single link is expected?
            if (isOccurs(property, Occurs.ZeroOrOne) || isOccurs(property, Occurs.ExactlyOne)) {
                if (links.isEmpty()) {
                    System.out.println("  link:  <none>");
                } else {
                    System.out.println("  link:  " + getDisplayLink(links.iterator().next()));
                }
            }

            // many links
            else {
                if (links.isEmpty()) {
                    System.out.println("  links: <none>");
                } else {
                    System.out.println("  links:");
                    for (Link link : links) {
                        System.out.println("    - " + getDisplayLink(link));
                    }
                }
            }
        }

        // primitive property
        else {
            // Note: use getObject() as, for a print out, we don't care whether the primitive is a String, a date or anything else.
            Object value = ResourceProperties.getObject(resource, property);
            if (value == null) {
                System.out.println("  value: <none>");
            } else if (value.getClass().isArray()) {
                System.out.println("  value: " + Arrays.asList((Object[]) value));
            } else {
                System.out.println("  value: " + value);
            }
        }
    }

    private static List<Property> getSortedProperties(ResourceShape shape) {
        List<Property> properties = new ArrayList<>(Arrays.asList(shape.getProperties()));

        Collections.sort(properties, new Comparator<Property>() {
            @Override
            public int compare(Property o1, Property o2) {

                // put primitive properties before links
                if (isResource(o1)) {
                    if (!isResource(o2)) {
                        return 1;
                    }
                } else if (isResource(o2)) {
                    return -1;
                }

                // compare titles
                return Collator.getInstance().compare(o1.getTitle(), o2.getTitle());
            }
        });

        return properties;
    }

    private static String getDisplayType(Property property) {
        StringBuilder buffer = new StringBuilder();

        // type name
        buffer.append(getDisplayTypeName(property));

        // occurs
        String occurs = getDisplayOccurs(property);
        if (!Strings.isNullOrEmpty(occurs)) {
            buffer.append(' ');
            buffer.append(occurs);
        }

        return buffer.toString();
    }

    private static String getDisplayTypeName(Property property) {
        if (isType(property, ValueType.Boolean)) {
            return "boolean";
        } else if (isType(property, ValueType.Date)) {
            return "date";
        } else if (isType(property, ValueType.DateTime)) {
            return "datetime";
        } else if (isType(property, ValueType.Decimal)) {
            return "decimal";
        } else if (isType(property, ValueType.Double)) {
            return "double";
        } else if (isType(property, ValueType.Float)) {
            return "float";
        } else if (isType(property, ValueType.Integer)) {
            return "integer";
        } else if (isType(property, ValueType.String)) {
            return "string";
        } else if (isType(property, ValueType.XMLLiteral)) {
            return "xml literal";
        } else if (isResource(property)) {
            return "resource";
        } else if (property.getValueType() == null) {
            return "<none>";
        } else {
            return property.getValueType().toString();
        }
    }

    private static boolean isResource(Property property) {
        return isType(property, ValueType.Resource);
    }

    private static boolean isType(Property property, ValueType type) {
        return type.toString().equals(String.valueOf(property.getValueType()));
    }

    private static String getDisplayOccurs(Property property) {
        if (isOccurs(property, Occurs.ZeroOrMany)) {
            return "[0..*]";
        } else if (isOccurs(property, Occurs.OneOrMany)) {
            return "[1..*]";
        } else {
            return "";
        }
    }

    private static boolean isOccurs(Property property, Occurs occurs) {
        return occurs.toString().equals(String.valueOf(property.getOccurs()));
    }

    private static String getDisplayLink(Link link) {
        String label = link.getLabel();
        if (Strings.isNullOrEmpty(label)) {
            return "<" + link.getValue() + ">";
        } else {
            return label + " <" + link.getValue() + ">";
        }
    }

}
