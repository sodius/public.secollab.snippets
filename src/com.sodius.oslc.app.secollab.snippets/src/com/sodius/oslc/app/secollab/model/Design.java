package com.sodius.oslc.app.secollab.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.lyo.oslc4j.core.annotation.OslcDescription;
import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNamespace;
import org.eclipse.lyo.oslc4j.core.annotation.OslcOccurs;
import org.eclipse.lyo.oslc4j.core.annotation.OslcPropertyDefinition;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRange;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.annotation.OslcTitle;
import org.eclipse.lyo.oslc4j.core.annotation.OslcValueType;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.OslcConstants;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import com.sodius.mdw.clm.common.model.Clm;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.Foaf;
import com.sodius.oslc.core.model.OslcCore;
import com.sodius.oslc.core.model.Rdfs;

@OslcNamespace(Clm.NAMESPACE_URI)
@OslcName("Design")
@OslcResourceShape(title = "CLM Design Resource Shape", describes = Clm.NAMESPACE_URI + "Design")
public class Design extends AbstractResource {

    private String description;
    private String identifier;
    private URI serviceProvider;
    private String title;
    private final Set<URI> creators = new TreeSet<>();
    private Date created;
    private URI modifiedBy;
    private Date modified;
    private URI details;
    private URI tool;
    private final List<Link> members = new ArrayList<>();
    private URI instanceShape;

    @OslcDescription("A unique identifier for a resource. Assigned by the service provider when a resource is created. Not intended for end-user display.")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "identifier")
    @OslcTitle("Identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    @OslcDescription("Title of the resource")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "title")
    @OslcTitle("Title")
    @OslcValueType(ValueType.XMLLiteral)
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @OslcDescription("Descriptive text (reference: Dublin Core) about resource represented as rich text in XHTML content.")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "description")
    @OslcTitle("Description")
    @OslcValueType(ValueType.XMLLiteral)
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @OslcDescription("Creator or creators of resource.")
    @OslcName("creator")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "creator")
    @OslcRange(Foaf.TYPE_PERSON)
    @OslcTitle("Creators")
    public URI[] getCreators() {
        return creators.toArray(new URI[creators.size()]);
    }

    public void setCreators(final URI[] creators) {
        this.creators.clear();

        if (creators != null) {
            this.creators.addAll(Arrays.asList(creators));
        }
    }

    public void addCreator(final URI creator) {
        this.creators.add(creator);
    }

    @OslcDescription("Timestamp of resource creation.")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "created")
    @OslcTitle("Created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @OslcDescription("The entity that most recently modified the subject resource.")
    @OslcName("modifiedBy")
    @OslcPropertyDefinition(OslcCore.NAMESPACE_URI + "modifiedBy")
    @OslcRange(Foaf.TYPE_PERSON)
    @OslcTitle("ModifiedBy")
    public URI getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(final URI modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @OslcDescription("Timestamp last latest resource modification.")
    @OslcPropertyDefinition(Dcterms.NAMESPACE_URI + "modified")
    @OslcTitle("Modified")
    public Date getModified() {
        return modified;
    }

    public void setModified(final Date modified) {
        this.modified = modified;
    }

    @OslcDescription("URL that may be used to retrieve web page to determine additional details about the resource")
    @OslcPropertyDefinition(OslcCore.NAMESPACE_URI + "details")
    @OslcTitle("Details")
    public URI getDetails() {
        return details;
    }

    public void setDetails(URI details) {
        this.details = details;
    }

    @OslcDescription("The scope of a resource is a URI for the resource's OSLC Service Provider.")
    @OslcPropertyDefinition(OslcCore.NAMESPACE_URI + "serviceProvider")
    @OslcRange(OslcConstants.TYPE_SERVICE_PROVIDER)
    @OslcTitle("Service Provider")
    public URI getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(final URI serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    @OslcOccurs(Occurs.ExactlyOne)
    @OslcPropertyDefinition(Clm.NAMESPACE_URI + "tool")
    public URI getTool() {
        return tool;
    }

    public void setTool(final URI tool) {
        this.tool = tool;
    }

    @OslcDescription("A member of the subject resource.")
    @OslcName("member")
    @OslcPropertyDefinition(Rdfs.NAMESPACE_URI + "member")
    @OslcTitle("Members")
    public Link[] getMembers() {
        return members.toArray(new Link[members.size()]);
    }

    public void setMembers(final Link[] members) {
        this.members.clear();

        if (members != null) {
            this.members.addAll(Arrays.asList(members));
        }
    }

    public void addMember(final Link member) {
        if (!this.members.contains(member)) {
            this.members.add(member);
        }
    }

    @OslcPropertyDefinition(OslcCore.NAMESPACE_URI + "instanceShape")
    @OslcTitle("Instance Shape")
    public URI getInstanceShape() {
        return instanceShape;
    }

    public void setInstanceShape(final URI instanceShape) {
        this.instanceShape = instanceShape;
    }

}
