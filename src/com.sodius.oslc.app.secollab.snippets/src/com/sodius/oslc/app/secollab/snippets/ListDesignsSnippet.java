package com.sodius.oslc.app.secollab.snippets;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;

import com.sodius.oslc.app.secollab.model.Design;
import com.sodius.oslc.app.secollab.model.Tool;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.client.requests.GetServiceProvider;
import com.sodius.oslc.core.util.Uris;
import com.sodius.oslc.domain.config.model.Component;
import com.sodius.oslc.domain.config.model.Configuration;
import com.sodius.oslc.domain.config.model.OslcConfig;

/**
 * <p>
 * Lists the Designs of a SECollab project.
 * </p>
 *
 * <p>
 * Required VM arguments:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: path to the Sodius license file in your file system</li>
 * <li><code>secollab.user</code>: SECollab User ID</li>
 * <li><code>secollab.password</code>: SECollab password</li>
 * <li><code>secollab.project</code>: URL of a SECollab service provider</li>
 * </ul>
 *
 * @see ListProjectsSnippet
 */
public class ListDesignsSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListDesignsSnippet().call();
    }

    private final Map<URI, Tool> tools;

    private ListDesignsSnippet() {
        this.tools = new HashMap<>();
    }

    @Override
    protected void call(OslcClient client) {

        // read project
        System.out.println("Reading project...");
        URI projectLocation = URI.create(getRequiredProperty("secollab.project"));
        ServiceProvider provider = new GetServiceProvider(client, projectLocation).get();

        // query components
        System.out.println("Querying components of '" + provider.getTitle() + "'...");
        Collection<Component> components = new GetResources<>(client, getComponentsLocation(provider), Component.class).get();

        // query configurations of each component
        for (Component component : components) {
            System.out.println();
            System.out.println();
            System.out.println("Querying Configurations of " + component.getTitle() + "'...");

            Collection<Configuration> configurations = new GetResources<>(client, getConfigurationsLocation(component), Configuration.class).get();

            // query Designs of each component in
            for (Configuration configuration : configurations) {
                System.out.println();
                System.out.println("Querying Designs of '" + component.getTitle() + "' in configuration '" + configuration.getTitle() + "'...");

                URI configuredComponentDesigns = OslcConfig.addContext(getDesignsLocation(component), configuration.getAbout());
                Collection<Design> designs = new GetResources<>(client, configuredComponentDesigns, Design.class).get();
                System.out.println("Project has " + designs.size() + " designs:");

                for (Design design : designs) {
                    System.out.println("- " + design.getTitle());
                    System.out.println("        URL : " + OslcConfig.addContext(design.getAbout(), configuration.getAbout()).toString());
                    System.out.println("        Tool: " + getTool(client, design.getTool()).getTitle());
                }
            }
        }
    }

    private static URI getComponentsLocation(ServiceProvider provider) {
        // @formatter:off
        return UriBuilder.fromUri(Uris.getContext(provider.getAbout()))
                .path("clm/services/component")
                .queryParam("provider", provider.getIdentifier())
                .build();
        // @formatter:on
    }

    private static URI getConfigurationsLocation(Component component) {
        // @formatter:off
        return UriBuilder.fromUri(component.getAbout())
                .path("configurations")
                .build();
        // @formatter:on
    }

    private static URI getDesignsLocation(Component component) {
        // @formatter:off
        return UriBuilder.fromUri(component.getAbout())
                .path("designs")
                .build();
        // @formatter:on
    }

    /*
     * Read on-demand a Design Tool and use a cache to not read twice the same tool.
     */
    private Tool getTool(OslcClient client, URI uri) {
        Tool tool = this.tools.get(uri);
        if (tool == null) {
            tool = new GetResource<>(client, uri, Tool.class).get();
            this.tools.put(uri, tool);
        }
        return tool;
    }
}
