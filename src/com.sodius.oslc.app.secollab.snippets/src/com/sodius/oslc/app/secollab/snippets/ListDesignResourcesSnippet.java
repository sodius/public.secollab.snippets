package com.sodius.oslc.app.secollab.snippets;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.ws.rs.core.UriBuilder;

import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;

import com.sodius.oslc.app.secollab.model.Design;
import com.sodius.oslc.app.secollab.model.Tool;
import com.sodius.oslc.client.OslcClient;
import com.sodius.oslc.client.requests.GetResource;
import com.sodius.oslc.client.requests.GetResources;
import com.sodius.oslc.core.model.Dcterms;
import com.sodius.oslc.core.model.ResourceProperties;
import com.sodius.oslc.domain.am.model.ArchitectureResource;
import com.sodius.oslc.domain.am.model.OslcAm;
import com.sodius.oslc.domain.config.model.OslcConfig;
import com.sodius.oslc.domain.rm.model.OslcRm;
import com.sodius.oslc.domain.rm.model.Requirement;

/**
 * <p>
 * Lists the Resources part of a Design.
 * </p>
 *
 * <p>
 * Required VM arguments:
 * </p>
 * <ul>
 * <li><code>sodius.license</code>: path to the Sodius license file in your file system</li>
 * <li><code>secollab.user</code>: SECollab User ID</li>
 * <li><code>secollab.password</code>: SECollab password</li>
 * <li><code>secollab.design</code>: URL of a SECollab Design</li>
 * </ul>
 *
 * @see ListDesignsSnippet
 */
public class ListDesignResourcesSnippet extends AbstractSnippet {

    public static void main(String[] args) throws Exception {
        new ListDesignResourcesSnippet().call();
    }

    @Override
    protected void call(OslcClient client) {

        // read design
        System.out.println("Reading design...");
        URI designLocation = URI.create(getRequiredProperty("secollab.design"));
        Design design = new GetResource<>(client, designLocation, Design.class).get();
        URI configurationAbout = OslcConfig.getContext(designLocation);

        // read tool
        Tool tool = new GetResource<>(client, design.getTool(), Tool.class).get();

        // query resources
        System.out.println("Querying resources of '" + design.getTitle() + "'...");
        Collection<IExtendedResource> resources = getResources(client, OslcConfig.addContext(getResourcesLocation(design), configurationAbout), tool);
        for (IExtendedResource resource : resources) {

            // load detailed version of the resource (the query respond with just a summary of properties)
            URI resourceAbout = OslcConfig.addContext(resource.getAbout(), configurationAbout);
            resource = getResource(client, resourceAbout, resource.getTypes());

            System.out.println("- " + ResourceProperties.getString(resource, Dcterms.PROPERTY_TITLE));
            System.out.println("        URL : " + resourceAbout);
            System.out.println("        Type: " + ResourceProperties.getStrings(resource, Dcterms.PROPERTY_TYPE).iterator().next());
        }
    }

    private static URI getResourcesLocation(Design design) {
        URI component = ResourceProperties.getURI(design, OslcConfig.PROPERTY_COMPONENT);

        // @formatter:off
        return UriBuilder.fromUri(component)
                .path("resources")
                .queryParam("designUri", design.getAbout())
                .queryParam("limit", Integer.toString(50)) // limit to 50 resources in the response
                .build();
        // @formatter:on
    }

    /*
     * Query for Requirements or Architecture Resources, depending on the domain declared by the tool.
     */
    private static Collection<IExtendedResource> getResources(OslcClient client, URI resourcesLocation, Tool tool) {
        Collection<IExtendedResource> resources = new ArrayList<>();
        Collection<URI> domains = Arrays.asList(tool.getDomains());

        // Architecture?
        if (domains.contains(URI.create(OslcAm.DOMAIN))) {
            for (ArchitectureResource resource : new GetResources<>(client, resourcesLocation, ArchitectureResource.class).get()) {
                resources.add(resource);
            }
        }

        // Requirements?
        if (domains.contains(URI.create(OslcRm.DOMAIN))) {
            for (Requirement resource : new GetResources<>(client, resourcesLocation, Requirement.class).get()) {
                resources.add(resource);
            }
        }

        return resources;
    }

    /*
     * Read a Requirement or Architecture Resource, depending on the declared RDF type.
     */
    private static IExtendedResource getResource(OslcClient client, URI uri, Collection<URI> types) {

        // Architecture?
        if (types.contains(URI.create(OslcAm.TYPE_AMRESOURCE)) || types.contains(URI.create(OslcAm.TYPE_DIAGRAM))) {
            return new GetResource<>(client, uri, ArchitectureResource.class).get();
        }

        // Requirements?
        else if (types.contains(URI.create(OslcRm.TYPE_REQUIREMENT)) || types.contains(URI.create(OslcRm.TYPE_REQUIREMENT_COLLECTION))) {
            return new GetResource<>(client, uri, Requirement.class).get();
        }

        else {
            throw new IllegalArgumentException("Unsupported types: " + types);
        }
    }
}
