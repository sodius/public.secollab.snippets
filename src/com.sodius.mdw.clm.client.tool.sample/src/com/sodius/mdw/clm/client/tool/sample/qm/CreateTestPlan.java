package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestPlan;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.TestPlan;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a test plan
 */
class CreateTestPlan extends AbstractCreateResource<TestPlan> {

    private final XmlTestPlan testPlan;

    CreateTestPlan(XmlTestPlan testPlan, Type type, Media icon, PublishContext context) {
        super("Create Test Plan: " + testPlan.getId(), type, icon, context);
        this.testPlan = testPlan;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Test Plan
        GUID guid = Identifiers.forTestPlanResource(testPlan);
        TestPlan resource = getType().createTestPlan(guid, testPlan.getName());
        resource.setShortTitle(testPlan.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(testPlan, resource);

        // create links
        createLink(resource, CreateTestPlanType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateTestPlanType.PROPERTY_RELEASES, testPlan.getReleases());
        createLinks(resource, CreateTestPlanType.PROPERTY_VALIDATES, testPlan.getValidates());
        createLinks(resource, CreateTestPlanType.PROPERTY_USES, testPlan.getUses());
    }

}
