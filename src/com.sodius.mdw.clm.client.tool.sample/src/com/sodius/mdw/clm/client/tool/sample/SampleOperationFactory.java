package com.sodius.mdw.clm.client.tool.sample;

import java.io.File;

import org.eclipse.jface.dialogs.IDialogSettings;

import com.sodius.mdw.clm.client.OperationFactory;
import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;

/**
 * The factory that creates the main operation to execute the publish.
 */
public class SampleOperationFactory implements OperationFactory {

    @Override
    public PublishOperation<?> createExport(File outputFile, IDialogSettings settings, PublishContext context) {
        return new SamplePublishOperation(outputFile, settings, context);
    }

}
