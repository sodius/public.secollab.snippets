package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the type for test execution record.
 */
class CreateTestExecutionRecordType extends PublishOperation<Type> {

    static final String PROPERTY_CONTAINER = "Container";

    private final Design design;

    CreateTestExecutionRecordType(Design design, PublishContext context) {
        super("Create Test Execution Record Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forTestExecutionRecordType();
        Type type = design.createType(guid, "Test Execution Record", "TestExecutionRecord");
        setResult(type);

        // create properties
        type.createResourceProperty(PROPERTY_CONTAINER).setContainer(true);
    }
}
