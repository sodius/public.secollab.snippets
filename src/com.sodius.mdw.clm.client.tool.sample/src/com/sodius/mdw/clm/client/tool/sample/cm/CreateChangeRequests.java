package com.sodius.mdw.clm.client.tool.sample.cm;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlChangeRequest;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the change requests.
 */
class CreateChangeRequests extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    CreateChangeRequests(Model sourceModel, Design design, PublishContext context) {
        super("Create Issues", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for change requests
        Type type = run(new CreateChangeRequestType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create icons for change requests
        Map<String, Media> icons = new HashMap<>();
        for (XmlChangeRequest changeRequest : XmlChangeRequest.getInstances(sourceModel)) {
            String iconType = changeRequest.getType();
            if (!icons.containsKey(iconType)) {
                Media icon = MediaFactory.getInstance(getPublishContext()).createChangeRequestIcon(iconType);
                icons.put(iconType, icon);
            }
        }

        // Step 3 - create a Resource for each Change Request
        for (XmlChangeRequest changeRequest : XmlChangeRequest.getInstances(sourceModel)) {
            Media icon = icons.get(changeRequest.getType());
            run(new CreateChangeRequest(changeRequest, type, icon, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 4 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }
}
