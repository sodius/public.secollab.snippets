package com.sodius.mdw.clm.client.tool.sample;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogSettings;

import com.sodius.mdw.clm.client.ProgressReportMonitor;
import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.ReadModelOperation;
import com.sodius.mdw.clm.client.tool.sample.ui.SamplePublishSetting;
import com.sodius.mdw.core.CoreException;
import com.sodius.mdw.core.util.progress.ProgressMonitor;
import com.sodius.mdw.metamodel.xml.XmlPackage;

/**
 * Reads the selected XML file into a model.
 */
class SampleReadModelOperation extends ReadModelOperation {

    SampleReadModelOperation(IDialogSettings settings, PublishContext context) {
        super(XmlPackage.eINSTANCE, settings, context);
    }

    @Override
    protected void readModel(IProgressMonitor monitor) {

        // retrieve selected filename to publish
        String filename = getSetting(SamplePublishSetting.SELECTED_FILE).getNonNullString();
        if (filename.isEmpty()) {
            getStatus().addError("An XML file to publish was not selected");
            return;
        }

        // read the XML file into a model
        try {
            Map<String, Object> options = new HashMap<>();
            options.put(ProgressMonitor.class.getName(), new ProgressReportMonitor(monitor));
            getResult().read("XML", filename, options);
        } catch (CoreException e) {
            getStatus().addError(e.getMessage(), e);
            return;
        }
    }

}
