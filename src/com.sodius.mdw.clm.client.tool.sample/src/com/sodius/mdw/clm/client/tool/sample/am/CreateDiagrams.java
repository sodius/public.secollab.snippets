package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagram;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create resources for Diagrams.
 */
class CreateDiagrams extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;
    private final AMResource owningPackage;

    CreateDiagrams(Model sourceModel, Design design, AMResource owningPackage, PublishContext context) {
        super("Create Diagrams", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
        this.owningPackage = owningPackage;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // loop on types of diagrams
        Media icon = MediaFactory.getInstance(getPublishContext()).createDiagramIcon();
        for (String typeName : XmlDiagram.getTypes(sourceModel)) {
            run(typeName, icon, monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }
    }

    private void run(String typeName, Media icon, IProgressMonitor monitor) {

        // Step 1 - Create a Type
        Type type = run(new CreateDiagramType(design, typeName, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each Diagram
        for (XmlDiagram diagram : XmlDiagram.getInstances(sourceModel, typeName)) {
            run(new CreateDiagram(diagram, type, icon, owningPackage, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 3 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }

}
