package com.sodius.mdw.clm.client.tool.sample.am;

import java.awt.Point;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagram;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagramEdge;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagramNode;
import com.sodius.mdw.clm.client.tool.sample.model.XmlElementWrapper;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Diagram;
import com.sodius.mdw.clm.common.publish.DiagramArea;
import com.sodius.mdw.clm.common.publish.DiagramPath;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Rectangle;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a resource for a Diagram.
 */
class CreateDiagram extends AbstractCreateResource<AMResource> {

    private final XmlDiagram diagram;
    private final AMResource owningPackage;

    CreateDiagram(XmlDiagram diagram, Type type, Media icon, AMResource owningPackage, PublishContext context) {
        super("Create Diagram: " + diagram.getId(), type, icon, context);
        this.diagram = diagram;
        this.owningPackage = owningPackage;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Resource
        GUID guid = Identifiers.forDiagramResource(diagram);
        Diagram resource = getType().createDiagram(guid, diagram.getName());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(diagram, resource);

        // create links
        resource.createValue(getType().getProperty(CreateItemType.PROPERTY_PACKAGE), owningPackage);

        // diagram image
        Media diagramImage = MediaFactory.getInstance(getPublishContext()).createDiagramImage(diagram);
        resource.setImage(diagramImage);

        // create diagram nodes
        for (XmlDiagramNode node : diagram.getNodes()) {
            Rectangle rectangle = resource.createRectangle(node.getX(), node.getY(), node.getWidth(), node.getHeight());
            setRepresents(rectangle, node.getItem());
        }

        // create diagram edges
        for (XmlDiagramEdge edge : diagram.getEdges()) {
            DiagramPath path = resource.createPath();
            for (Point point : edge.getPoints()) {
                path.createCoordinate((int) point.getX(), (int) point.getY());
            }
            setRepresents(path, edge.getItem());
        }
    }

    private void setRepresents(DiagramArea area, XmlElementWrapper targetElement) {

        // The target element may not yet have a corresponding resource in the Publisher model.
        // Register a Runnable to execute only once the targetElement has a known corresponding resource being created.
        // (registration is done by using the Mapping.put() method when creating a resource)
        // (Runnable instances are executed on-demand using Mapping.getInstance(context).createBuilder(), see CreateDesign)
        Mapping.getInstance(getPublishContext()).add(new Runnable() {

            @Override
            public void run() {
                Resource targetResource = (Resource) Mapping.getInstance(getPublishContext()).get(targetElement);
                area.setRepresents(targetResource);
            }
        });
    }
}
