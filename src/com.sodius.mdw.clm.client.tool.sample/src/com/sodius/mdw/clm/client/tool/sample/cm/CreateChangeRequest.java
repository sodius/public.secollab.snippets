package com.sodius.mdw.clm.client.tool.sample.cm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlChangeRequest;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.ChangeRequest;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a change request.
 */
class CreateChangeRequest extends AbstractCreateResource<ChangeRequest> {

    private final XmlChangeRequest changeRequest;

    CreateChangeRequest(XmlChangeRequest changeRequest, Type type, Media icon, PublishContext context) {
        super("Create Issue: " + changeRequest.getId(), type, icon, context);
        this.changeRequest = changeRequest;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Change Request
        GUID guid = Identifiers.forChangeRequestResource(changeRequest);
        ChangeRequest resource = getType().createChangeRequest(guid, changeRequest.getName());
        resource.setShortTitle(changeRequest.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(changeRequest, resource);

        // set attribute values
        String type = changeRequest.getType();
        if (type != null) {
            resource.createValue(getType().getProperty(CreateChangeRequestType.PROPERTY_TYPE), type);
        }

        String priority = changeRequest.getPriority();
        if (priority != null) {
            resource.createValue(getType().getProperty(CreateChangeRequestType.PROPERTY_PRIORITY), priority);
        }

        String status = changeRequest.getStatus();
        if (status != null) {
            resource.createValue(getType().getProperty(CreateChangeRequestType.PROPERTY_STATUS), status);
        }

        // create links
        createLink(resource, CreateChangeRequestType.PROPERTY_CONTAINER, CreateChangeRequestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateChangeRequestType.PROPERTY_CAUSES, changeRequest.getCauses());
        createLinks(resource, CreateChangeRequestType.PROPERTY_CAUSED_BY, changeRequest.getCausedBy());
        createLinks(resource, CreateChangeRequestType.PROPERTY_BLOCKS, changeRequest.getBlocks());
        createLinks(resource, CreateChangeRequestType.PROPERTY_BLOCKED_BY, changeRequest.getBlockedBy());
    }

}
