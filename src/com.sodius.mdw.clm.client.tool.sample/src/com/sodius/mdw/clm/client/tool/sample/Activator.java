package com.sodius.mdw.clm.client.tool.sample;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * A class instantiated only once, the first time the plug-in (a.k.a the bundle) is being used.
 * (referenced by Bundle-Activator in the MANIFEST.MF file)
 */
public class Activator implements BundleActivator {

    private static BundleContext context;

    /**
     * Gives access to the context in which the plug-in is running.
     */
    static BundleContext getContext() {
        return context;
    }

    /**
     * Called the plug-in is activated
     */
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        Activator.context = bundleContext;
    }

    /**
     * Called when the plug-in is stopped (i.e. the publisher is closed)
     */
    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        Activator.context = null;
    }

}
