package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the type for test plan.
 */
class CreateTestPlanType extends PublishOperation<Type> {

    static final String PROPERTY_CONTAINER = "Container";
    static final String PROPERTY_RELEASES = "Releases";
    static final String PROPERTY_VALIDATES = "Validates Requirements";
    static final String PROPERTY_USES = "Uses Test Cases";

    private final Design design;

    CreateTestPlanType(Design design, PublishContext context) {
        super("Create Test Plan Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forTestPlanType();
        Type type = design.createType(guid, "Test Plan", "TestPlan");
        setResult(type);

        // create properties
        type.createResourceProperty(PROPERTY_CONTAINER).setContainer(true);
        type.createResourcesProperty(PROPERTY_RELEASES);
        type.createResourcesProperty(PROPERTY_VALIDATES);
        type.createResourcesProperty(PROPERTY_USES);
    }
}
