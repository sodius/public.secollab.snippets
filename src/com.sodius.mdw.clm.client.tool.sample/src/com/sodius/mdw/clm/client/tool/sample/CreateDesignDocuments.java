package com.sodius.mdw.clm.client.tool.sample;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.am.CreatePackages;
import com.sodius.mdw.clm.client.tool.sample.cm.CreateChangeRequestContainers;
import com.sodius.mdw.clm.client.tool.sample.qm.CreateTestContainers;
import com.sodius.mdw.clm.client.tool.sample.rm.CreateSpecifications;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.core.model.Model;

/**
 * Create the documents, their contained resources and pages.
 */
class CreateDesignDocuments extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    CreateDesignDocuments(Model sourceModel, Design design, PublishContext context) {
        super("Create Design Documents", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {
        Media folderIcon = MediaFactory.getInstance(getPublishContext()).createFolderIcon();

        // create AM resources
        run(new CreatePackages(sourceModel, design, folderIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // create RM resources
        run(new CreateSpecifications(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // create CM resources
        run(new CreateChangeRequestContainers(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // create QM resources
        run(new CreateTestContainers(sourceModel, design, folderIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

    }
}
