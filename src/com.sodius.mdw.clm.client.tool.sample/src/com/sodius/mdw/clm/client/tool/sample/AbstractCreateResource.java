package com.sodius.mdw.clm.client.tool.sample;

import java.util.List;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.model.XmlElementWrapper;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Property;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.ResourceValue;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Base functionalities to create a resource.
 */
public abstract class AbstractCreateResource<R extends Resource> extends PublishOperation<R> {

    private final Type type;
    private final Media icon;

    public AbstractCreateResource(String operationName, Type type, Media icon, PublishContext context) {
        super(operationName, context.getSettings(), context);
        this.type = type;
        this.icon = icon;
    }

    protected final Type getType() {
        return type;
    }

    protected final Media getIcon() {
        return icon;
    }

    protected final void createLinks(Resource resource, String propertyTitle, List<? extends XmlElementWrapper> targetElements) {
        for (XmlElementWrapper targetElement : targetElements) {
            createLink(resource, propertyTitle, targetElement);
        }
    }

    protected final void createLink(Resource resource, String propertyTitle, Object targetElement) {
        if (targetElement != null) {

            // The target element may not yet have a corresponding resource in the Publisher model.
            // Register a Runnable to execute only once the targetElement has a known corresponding resource being created.
            // (registration is done by using the Mapping.put() method when creating a resource)
            // (Runnable instances are executed on-demand using Mapping.getInstance(context).createBuilder(), see CreateDesign)
            Mapping.getInstance(getPublishContext()).add(new Runnable() {

                @Override
                public void run() {

                    // get the resource now associated to the target element
                    Resource targetResource = (Resource) Mapping.getInstance(getPublishContext()).get(targetElement);

                    // get the Property defined on the Type
                    Property property = resource.getType().getProperty(propertyTitle);

                    // add the association between the two resources
                    ResourceValue value = (ResourceValue) resource.getValue(property);
                    if (value == null) {
                        resource.createValue(property, targetResource);
                    } else {
                        value.getValues().add(targetResource);
                    }
                }
            });
        }
    }
}
