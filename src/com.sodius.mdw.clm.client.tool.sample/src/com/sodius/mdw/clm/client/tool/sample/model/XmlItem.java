package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlItem extends XmlElementWrapper {

    public static Collection<String> getTypes(Model model) {
        Collection<String> result = new HashSet<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("item".equals(element.getTagName())) {
                result.add(element.getAttribute("type"));
            }
        }
        return result;
    }

    public static Collection<XmlItem> getInstances(Model model, String type) {
        Collection<XmlItem> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("item".equals(element.getTagName()) && type.equals(element.getAttribute("type"))) {
                result.add(new XmlItem(element));
            }
        }
        return result;
    }

    XmlItem(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public String getType() {
        return getElement().getAttribute("type");
    }

    public List<XmlElementWrapper> getImplements() {
        return getReferencedElements("implements");
    }

    public List<XmlElementWrapper> getElaborates() {
        return getReferencedElements("elaborates");
    }

    public List<XmlElementWrapper> getConnects() {
        return getReferencedElements("connects");
    }
}
