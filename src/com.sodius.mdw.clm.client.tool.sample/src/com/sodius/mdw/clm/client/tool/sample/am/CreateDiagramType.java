package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a Type of Diagram.
 */
class CreateDiagramType extends PublishOperation<Type> {

    static final String PROPERTY_PACKAGE = "Package";

    private final Design design;
    private final String name;

    CreateDiagramType(Design design, String name, PublishContext context) {
        super("Create Type: " + name, context.getSettings(), context);
        this.design = design;
        this.name = name;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forDiagramType(name);
        Type type = design.createType(guid, name, name);
        setResult(type);

        // create properties
        type.createResourceProperty(PROPERTY_PACKAGE).setContainer(true);
    }
}
