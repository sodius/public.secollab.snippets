package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlTestCase extends XmlElementWrapper {

    public static Collection<XmlTestCase> getInstances(Model model) {
        Collection<XmlTestCase> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("testCase".equals(element.getTagName())) {
                result.add(new XmlTestCase(element));
            }
        }
        return result;
    }

    XmlTestCase(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public List<XmlElementWrapper> getValidates() {
        return getReferencedElements("validates");
    }

    public List<XmlElementWrapper> getUses() {
        return getReferencedElements("uses");
    }

    public List<XmlElementWrapper> getUsedBy() {
        return getReferencedElements("usedBy");
    }

}
