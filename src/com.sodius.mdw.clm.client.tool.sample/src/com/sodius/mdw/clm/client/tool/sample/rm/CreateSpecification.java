package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlSpecification;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.RequirementCollection;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the specification documents, their contained resources and pages.
 */
class CreateSpecification extends PublishOperation<RequirementCollection> {

    private final XmlSpecification specification;
    private final Type type;
    private final Media icon;

    CreateSpecification(XmlSpecification specification, Type type, Media icon, PublishContext context) {
        super("Create Specification: " + specification.getId(), context.getSettings(), context);
        this.specification = specification;
        this.type = type;
        this.icon = icon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a RequirementCollection
        GUID guid = Identifiers.forSpecificationResource(specification);
        RequirementCollection resource = type.createRequirementCollection(guid, specification.getName());
        resource.setIcon(icon);
        setResult(resource);

        // register it as a document of the design
        type.getDesign().getDocuments().add(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(specification, resource);

        // set attribute values
        resource.createValue(type.getProperty(CreateSpecificationType.PROPERTY_STATUS), specification.getStatus());
    }
}
