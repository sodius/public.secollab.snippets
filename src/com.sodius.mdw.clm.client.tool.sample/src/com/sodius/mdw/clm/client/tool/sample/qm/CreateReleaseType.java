package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the type for release.
 */
class CreateReleaseType extends PublishOperation<Type> {
    static final String TYPE_NAME = "Release";
    static final String PROPERTY_CONTAINER = "Container";
    static final String PROPERTY_VALIDATED_BY = "Validated By";

    private final Design design;

    CreateReleaseType(Design design, PublishContext context) {
        super("Create TRelease Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forReleaseType();
        Type type = design.createType(guid, "Release", TYPE_NAME);
        setResult(type);

        // create properties
        type.createResourceProperty(PROPERTY_CONTAINER).setContainer(true);
        type.createResourcesProperty(PROPERTY_VALIDATED_BY);
    }
}
