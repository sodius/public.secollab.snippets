package com.sodius.mdw.clm.client.tool.sample.cm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.PrimitiveType;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the type for change request.
 */
class CreateChangeRequestType extends PublishOperation<Type> {

    static final String PROPERTY_TYPE = "Type";
    static final String PROPERTY_PRIORITY = "Priority";
    static final String PROPERTY_STATUS = "Status";
    static final String PROPERTY_CONTAINER = "Container";
    static final String PROPERTY_CAUSED_BY = "Caused By";
    static final String PROPERTY_CAUSES = "Causes";
    static final String PROPERTY_BLOCKED_BY = "Blocked By";
    static final String PROPERTY_BLOCKS = "Blocks";

    private final Design design;

    CreateChangeRequestType(Design design, PublishContext context) {
        super("Create Issue Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forChangeRequestType();
        Type type = design.createType(guid, "Issue", "Issue");
        setResult(type);

        // create properties
        type.createPrimitiveProperty(PROPERTY_TYPE, PrimitiveType.STRING);
        type.createPrimitiveProperty(PROPERTY_PRIORITY, PrimitiveType.STRING);
        type.createPrimitiveProperty(PROPERTY_STATUS, PrimitiveType.STRING);
        type.createResourceProperty(PROPERTY_CONTAINER).setContainer(true);
        type.createResourcesProperty(PROPERTY_CAUSED_BY);
        type.createResourcesProperty(PROPERTY_CAUSES);
        type.createResourcesProperty(PROPERTY_BLOCKED_BY);
        type.createResourcesProperty(PROPERTY_BLOCKS);
    }
}
