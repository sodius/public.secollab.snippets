package com.sodius.mdw.clm.client.tool.sample;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.OSLCDomain;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Tool;
import com.sodius.mdw.core.model.Model;

/**
 * Analyzes the authoring data loaded in the source model and creates corresponding resources in the Publisher model.
 */
class CreateDesign extends PublishOperation<Design> {

    private final Model sourceModel;
    private final Model publisherModel;

    CreateDesign(Model sourceModel, Model publisherModel, PublishContext context) {
        super("Create Design", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.publisherModel = publisherModel;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - create Design resource
        Design design = createDesign();

        // Step 2 - create Tool resource
        createTool(design);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 3 - create Design documents
        run(new CreateDesignDocuments(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 4 - create references in the publisher model, executing the registered Runnable instances
        // (Runnables were registered in nested operations using Mapping.getInstances(context).add(runnable) method)
        run(Mapping.getInstance(getPublishContext()).createBuilder(), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
    }

    private Design createDesign() {

        // create singleton design instance
        Design design = publisherModel.create(PublishPackage.Literals.DESIGN);
        setResult(design);

        // register a shared factory to create Media instances, now that the design is created
        MediaFactory.createInstance(getPublishContext(), design);

        return design;
    }

    private void createTool(Design design) {

        // create a Tool resource, registered on the design
        Tool tool = design.eModel().create(PublishPackage.Literals.TOOL);
        design.setTool(tool);

        // title is displayed to the end user in the web pages (should be the authoringTool name registered in the plugin.xml file)
        tool.setTitle("Sample");

        // identifier should be the one declared in the com.sodius.mdw.clm.client.authoringTool extension
        tool.setIdentifier("com.sodius.mdw.clm.client.tool.sample");

        // namespace is used to store corresponding RDF resources
        tool.setNamespacePrefix("mytool");
        tool.setNamespace(URI.createURI("http://www.mycompany.com/ns/mytool#"));

        // the OSLC domains this authoring tool is using (both AM and RM in this sample)
        tool.getDomains().add(OSLCDomain.OSLC_AM); // Architecture Management
        tool.getDomains().add(OSLCDomain.OSLC_CM); // Change Management
        tool.getDomains().add(OSLCDomain.OSLC_QM); // Quality Management
        tool.getDomains().add(OSLCDomain.OSLC_RM); // Requirements Management

        // the icon is displayed to the end user in the web pages (should be the authoringTool icon registered in the plugin.xml file)
        Media icon = MediaFactory.getInstance(getPublishContext()).createToolIcon();
        tool.setIcon(icon);
    }

}
