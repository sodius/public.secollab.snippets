package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlSpecification extends XmlElementWrapper {

    public static Collection<XmlSpecification> getInstances(Model model) {
        Collection<XmlSpecification> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("specification".equals(element.getTagName())) {
                result.add(new XmlSpecification(element));
            }
        }
        return result;
    }

    XmlSpecification(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public String getStatus() {
        return getElement().getAttribute("status");
    }

    public List<XmlRequirement> getRootRequirements() {
        List<XmlRequirement> result = new ArrayList<>();
        for (Object content : getElement().getContent()) {
            if (content instanceof Element) {
                Element contentElement = (Element) content;
                if ("requirement".equals(contentElement.getTagName())) {
                    result.add(new XmlRequirement(contentElement));
                }
            }
        }
        return result;
    }
}
