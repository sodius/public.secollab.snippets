package com.sodius.mdw.clm.client.tool.sample.model;

import com.sodius.mdw.metamodel.xml.Element;

public class XmlDiagramNode extends XmlElementWrapper {

    XmlDiagramNode(Element element) {
        super(element);
    }

    public XmlElementWrapper getItem() {
        return getReferencedAttribute("idref");
    }

    public int getX() {
        return Integer.parseInt(getElement().getAttribute("x"));
    }

    public int getY() {
        return Integer.parseInt(getElement().getAttribute("y"));
    }

    public int getWidth() {
        return Integer.parseInt(getElement().getAttribute("width"));
    }

    public int getHeight() {
        return Integer.parseInt(getElement().getAttribute("height"));
    }
}
