package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestCase;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.TestCase;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a test case
 */
class CreateTestCase extends AbstractCreateResource<TestCase> {

    private final XmlTestCase testCase;

    CreateTestCase(XmlTestCase testCase, Type type, Media icon, PublishContext context) {
        super("Create Test Case: " + testCase.getId(), type, icon, context);
        this.testCase = testCase;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Test Case
        GUID guid = Identifiers.forTestCaseResource(testCase);
        TestCase resource = getType().createTestCase(guid, testCase.getName());
        resource.setShortTitle(testCase.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(testCase, resource);

        // create links
        createLink(resource, CreateTestCaseType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateTestCaseType.PROPERTY_VALIDATES, testCase.getValidates());
        createLinks(resource, CreateTestCaseType.PROPERTY_USES, testCase.getUses());
        createLinks(resource, CreateTestCaseType.PROPERTY_USED_BY, testCase.getUsedBy());
    }

}
