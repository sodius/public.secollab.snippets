package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a Type of Item.
 */
class CreateItemType extends PublishOperation<Type> {

    static final String PROPERTY_PACKAGE = "Package";
    static final String PROPERTY_ELABORATES = "Elaborates";
    static final String PROPERTY_IMPLEMENTS = "Implements";
    static final String PROPERTY_CONNECTS = "Connects";

    private final Design design;
    private final String name;

    CreateItemType(Design design, String name, PublishContext context) {
        super("Create Type: " + name, context.getSettings(), context);
        this.design = design;
        this.name = name;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forItemType(name);
        Type type = design.createType(guid, name, name);
        setResult(type);

        // create properties
        type.createResourceProperty(PROPERTY_PACKAGE).setContainer(true);
        type.createResourcesProperty(PROPERTY_ELABORATES);
        type.createResourcesProperty(PROPERTY_IMPLEMENTS);
        type.createResourcesProperty(PROPERTY_CONNECTS);
    }
}
