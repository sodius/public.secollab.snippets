package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlItem;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create an AM resource for an Item.
 */
class CreateItem extends AbstractCreateResource<AMResource> {

    private final XmlItem item;
    private final AMResource owningPackage;

    CreateItem(XmlItem item, Type type, Media icon, AMResource owningPackage, PublishContext context) {
        super("Create Item: " + item.getId(), type, icon, context);
        this.item = item;
        this.owningPackage = owningPackage;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Resource
        GUID guid = Identifiers.forItemResource(item);
        AMResource resource = getType().createAMResource(guid, item.getName());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(item, resource);

        // create links
        resource.createValue(getType().getProperty(CreateItemType.PROPERTY_PACKAGE), owningPackage);
        createLinks(resource, CreateItemType.PROPERTY_ELABORATES, item.getElaborates());
        createLinks(resource, CreateItemType.PROPERTY_IMPLEMENTS, item.getImplements());
        createLinks(resource, CreateItemType.PROPERTY_CONNECTS, item.getConnects());
    }
}
