package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestScript;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.TestScript;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a test script
 */
class CreateTestScript extends AbstractCreateResource<TestScript> {

    private final XmlTestScript testScript;

    CreateTestScript(XmlTestScript testScript, Type type, Media icon, PublishContext context) {
        super("Create Test Script: " + testScript.getId(), type, icon, context);
        this.testScript = testScript;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Test Script
        GUID guid = Identifiers.forTestScriptResource(testScript);
        TestScript resource = getType().createTestScript(guid, testScript.getName());
        resource.setShortTitle(testScript.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(testScript, resource);

        // create links
        createLink(resource, CreateTestScriptType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateTestScriptType.PROPERTY_USED_BY, testScript.getUsedBy());
    }

}
