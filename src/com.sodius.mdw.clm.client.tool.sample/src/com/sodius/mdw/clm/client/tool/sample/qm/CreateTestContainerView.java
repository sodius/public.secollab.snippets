package com.sodius.mdw.clm.client.tool.sample.qm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.ExplorerPage;
import com.sodius.mdw.clm.common.publish.ExplorerView;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Row;
import com.sodius.mdw.clm.common.publish.Table;
import com.sodius.mdw.clm.common.publish.Tree;
import com.sodius.mdw.clm.common.publish.TreeNode;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.clm.common.publish.XMLLiteralUtils;
import com.sodius.mdw.clm.common.publish.operations.TitleComparator;

/**
 * Creates an explorer view to display test resources.
 */
class CreateTestContainerView extends PublishOperation<ExplorerView> {

    private final Design design;
    private final ExplorerPage page;
    private final Media folderIcon;

    public CreateTestContainerView(Design design, ExplorerPage page, Media folderIcon, PublishContext context) {
        super("Create Test Container View", context.getSettings(), context);
        this.design = design;
        this.page = page;
        this.folderIcon = folderIcon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // view
        ExplorerView view = page.createTreeView(Identifiers.forExplorerTestsView(), "Tests");
        Tree tree = (Tree) view.getToc();
        setResult(view);

        // test resources
        createTestSection(tree, "Test Plans", PublishPackage.Literals.TEST_PLAN);
        createTestSection(tree, "Test Cases", PublishPackage.Literals.TEST_CASE);
        createTestSection(tree, "Test Scripts", PublishPackage.Literals.TEST_SCRIPT);
        createTestSection(tree, "Test Execution Records", PublishPackage.Literals.TEST_EXECUTION_RECORD);
        createTestSection(tree, "Test Results", PublishPackage.Literals.TEST_RESULT);
        createTestSection(tree, "Releases", getTestType(CreateReleaseType.TYPE_NAME));
    }

    private void createTestSection(Tree tree, String title, EClass clazz) {

        // find type
        Type type = getTestType(clazz);
        if (type == null) {
            getStatus().addError("No such type: " + clazz.getName());
            return;
        }

        createTestSection(tree, title, type);
    }

    private void createTestSection(Tree tree, String title, Type type) {
        // test type tree node
        TreeNode typeNode = tree.createNode(title);
        typeNode.setIcon(folderIcon);

        // table of test resources
        Table table = createTable(type, title);
        typeNode.setTable(table);

        // sort tests
        List<Resource> tests = new ArrayList<>(type.getResources());
        Collections.sort(tests, new TitleComparator());

        // child nodes
        for (Resource test : tests) {

            // test tree node
            typeNode.createNode(test);

            // test table row
            Row row = table.createRow(test);
            row.createCell(getTestCellValue(test));
        }
    }

    private Table createTable(Type type, String columnName) {
        GUID guid = GUID.valueOf(getResult().getToc().getGuid()).append(type.getGuid());
        Table table = design.eModel().create(PublishPackage.Literals.TABLE);
        table.setGuid(guid.toString());
        table.createColumn(columnName);
        return table;
    }

    private Type getTestType(EClass clazz) {
        for (Type type : design.getTypes()) {
            if (!type.getResources().isEmpty() && (clazz.isInstance(type.getResources().first()))) {
                return type;
            }
        }

        return null;
    }

    private Type getTestType(String name) {
        return design.getTypes().stream().filter(type -> name.equals(type.getName())).findFirst().orElse(null);
    }

    private String getTestCellValue(Resource resource) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(XMLLiteralUtils.HTML_BODY_START);
        buffer.append(XMLLiteralUtils.htmlImage(resource.getIcon()));
        buffer.append(XMLLiteralUtils.HTML_NON_BREAKING_SPACE);
        buffer.append(XMLLiteralUtils.toXMLLiteral(resource.getTitle()));
        buffer.append(XMLLiteralUtils.HTML_BODY_END);
        return buffer.toString();
    }
}
