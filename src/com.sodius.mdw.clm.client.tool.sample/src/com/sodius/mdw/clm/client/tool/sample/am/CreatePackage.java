package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create an AM package.
 */
class CreatePackage extends PublishOperation<AMResource> {

    private final Type type;
    private final Media icon;

    CreatePackage(Type type, Media icon, PublishContext context) {
        super("Create Package", context.getSettings(), context);
        this.type = type;
        this.icon = icon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Resource
        String name = "Architecture Resources";
        GUID guid = Identifiers.forPackageResource(name);
        AMResource resource = type.createAMResource(guid, name);
        resource.setIcon(icon);
        setResult(resource);

        // register it as a document of the design
        type.getDesign().getDocuments().add(resource);
    }
}
