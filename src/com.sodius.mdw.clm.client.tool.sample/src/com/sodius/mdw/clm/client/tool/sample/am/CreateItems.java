package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlItem;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create resources for Items.
 */
class CreateItems extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;
    private final AMResource owningPackage;

    CreateItems(Model sourceModel, Design design, AMResource owningPackage, PublishContext context) {
        super("Create Items", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
        this.owningPackage = owningPackage;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // loop on types of items
        for (String typeName : XmlItem.getTypes(sourceModel)) {
            run(typeName, monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }
    }

    private void run(String typeName, IProgressMonitor monitor) {

        // Step 1 - Create a Type
        Media icon = MediaFactory.getInstance(getPublishContext()).createItemIcon(typeName);
        Type type = run(new CreateItemType(design, typeName, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each Item
        for (XmlItem item : XmlItem.getInstances(sourceModel, typeName)) {
            run(new CreateItem(item, type, icon, owningPackage, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 3 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }

}
