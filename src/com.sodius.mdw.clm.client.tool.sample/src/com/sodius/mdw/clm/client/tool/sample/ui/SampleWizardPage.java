package com.sodius.mdw.clm.client.tool.sample.ui;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Text;

import com.sodius.mdw.clm.client.PublishSetting;
import com.sodius.mdw.clm.client.ui.PublishWizardPage;
import com.sodius.mdw.clm.client.ui.SWTUtils;

/**
 * A page enabling the user to select a file to publish.
 */
class SampleWizardPage extends PublishWizardPage {

    private Text fileText;

    public SampleWizardPage() {
        super("file-selection");
        setTitle("Sample Content");
        setDescription("Select the sample XML file to publish into SECollab.");
    }

    @Override
    public void createControl(Composite parent) {

        // create controls
        Composite composite = new Composite(parent, SWT.NULL);
        composite.setLayout(new GridLayout());
        setControl(composite);
        createFileSelectionArea(composite);

        // restore previous selection, if any
        String previousFilename = getSetting(SamplePublishSetting.SELECTED_FILE).getString();
        if (previousFilename != null) {
            this.fileText.setText(previousFilename);
        }

        // display an error, if any
        validate();
    }

    protected void createFileSelectionArea(Composite parent) {
        Composite composite = SWTUtils.createComposite(parent);
        SWTUtils.setGridLayout(composite, 3, false, false);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

        // text
        this.fileText = createText(composite, "File:", SWT.BORDER);

        // browse button
        Button browseFileSystemButton = SWTUtils.createPushButton(composite, "Browse...");
        browseFileSystemButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
            @Override
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {

                // configure a file selection dialog
                FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
                dialog.setText(getTitle());
                dialog.setFilterNames(new String[] { "XML Files (*.xml)", "All Files (*.*)" });
                dialog.setFilterExtensions(new String[] { "*.xml", "*.*" });

                // open
                String selectedFilename = dialog.open();
                if (selectedFilename != null) {

                    // update the field
                    fileText.setText(selectedFilename);
                }
            }
        });
    }

    @Override
    protected String computeStatus() {
        String filename = this.fileText.getText();

        // empty ?
        if (filename.length() == 0) {
            return ""; // no error message, but cannot complete the page
        }

        File file = new File(filename);

        // directory ?
        if (file.isDirectory()) {
            return "This file is a directory";
        }

        // ! exists ?
        if (!file.exists()) {
            return "The file does not exist";
        }

        // ! absolute ?
        if (!file.isAbsolute()) {
            return "The file path must be absolute";
        }

        return null; // all good
    }

    @Override
    protected void saveValues() {

        // store the file selected by user
        String filename = this.fileText.getText();
        getSetting(SamplePublishSetting.SELECTED_FILE).set(filename);

        // store a suggested name for the design, that the user can modify later on
        String name = new File(filename).getName();
        getSetting(PublishSetting.DESIGN_NAME).set(name);
    }

}
