package com.sodius.mdw.clm.client.tool.sample.cm;

import java.util.Collection;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlChangeRequest;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Row;
import com.sodius.mdw.clm.common.publish.Table;
import com.sodius.mdw.clm.common.publish.TablePage;
import com.sodius.mdw.clm.common.publish.TableView;
import com.sodius.mdw.core.model.Model;

/**
 * Create a table page showing all change requests
 */
class CreateChangeRequestContainerPage extends PublishOperation<TablePage> {

    private final Model sourceModel;

    CreateChangeRequestContainerPage(Model sourceModel, PublishContext context) {
        super("Create Page for Issues", context.getSettings(), context);
        this.sourceModel = sourceModel;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create page
        Resource resource = (Resource) Mapping.getInstance(getPublishContext()).get(CreateChangeRequestContainer.CONTAINER_INPUT);
        TablePage page = resource.eModel().create(PublishPackage.Literals.TABLE_PAGE);
        resource.setPage(page);
        setResult(page);

        // create table
        GUID guid = Identifiers.forChangeRequestContainerTable();
        TableView tableView = page.createView(guid, "Change Requests");
        Table table = tableView.getTable();

        // create columns
        table.createColumn("ID").setHintWidth("120px");
        table.createColumn("Type").setHintWidth("120px");
        table.createColumn("Text");
        table.createColumn("Priority").setHintWidth("100px");
        table.createColumn("Status").setHintWidth("120px");

        // create rows
        createRows(table, XmlChangeRequest.getInstances(sourceModel));

        // use the same table for document reporting on this specification
        page.setReportView(tableView);
    }

    private void createRows(Table table, Collection<XmlChangeRequest> changeRequests) {

        // loop on requirements
        for (XmlChangeRequest changeRequest : changeRequests) {
            Resource resource = (Resource) Mapping.getInstance(getPublishContext()).get(changeRequest);

            // create row
            Row row = table.createRow(resource);
            row.createCell(changeRequest.getId());
            row.createCell(changeRequest.getType());
            row.createCell(changeRequest.getName());
            row.createCell(changeRequest.getPriority());
            row.createCell(changeRequest.getStatus());
        }

    }
}
