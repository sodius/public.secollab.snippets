package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.ExplorerPage;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.core.model.Model;

/**
 * Creates an explorer page that contains a <b>Diagrams</b> view and a <b>Types</b> view.
 */
class CreatePackagePage extends PublishOperation<ExplorerPage> {

    private final Resource resource;
    private final Media folderIcon;

    public CreatePackagePage(Resource resource, Media folderIcon, PublishContext context) {
        super("Create Explorer Page", context.getSettings(), context); //$NON-NLS-1$
        this.resource = resource;
        this.folderIcon = folderIcon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // page
        Model publisherModel = resource.eModel();
        ExplorerPage page = publisherModel.create(PublishPackage.Literals.EXPLORER_PAGE);
        resource.setPage(page);
        setResult(page);

        // views
        Design design = resource.getType().getDesign();
        run(new CreateDiagramsView(design, page, folderIcon, getPublishContext()), monitor);
        run(new CreateTypesView(design, page, getPublishContext()), monitor);

        // report view -> diagrams
        page.setReportView(page.getViews().first());
    }
}
