package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.PrimitiveType;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the Type for specifications.
 */
class CreateSpecificationType extends PublishOperation<Type> {

    static final String PROPERTY_STATUS = "Status";

    private final Design design;

    CreateSpecificationType(Design design, PublishContext context) {
        super("Create Specification Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forSpecificationType();
        Type type = design.createType(guid, "Specification", "Specification");
        setResult(type);

        // create properties
        type.createPrimitiveProperty(PROPERTY_STATUS, PrimitiveType.STRING);
    }
}
