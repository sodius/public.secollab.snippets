package com.sodius.mdw.clm.client.tool.sample;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.common.publish.IdentifiedElement;
import com.sodius.mdw.core.operations.Operation;

/**
 * Provides facilities to store information related to the mapping between elements from the input data to resources part of the published model.
 */
public class Mapping {

    /**
     * Obtain the shared mapping for the specified context.
     */
    public static synchronized Mapping getInstance(PublishContext context) {
        Mapping references = (Mapping) context.getProperties().getProperty(Mapping.class.getName());
        if (references == null) {
            references = new Mapping();
            context.getProperties().setProperty(Mapping.class.getName(), references);
        }
        return references;
    }

    private final Map<Object, IdentifiedElement> mappings;
    private final LinkedList<Runnable> runnables;

    private Mapping() {
        this.mappings = new HashMap<>();
        this.runnables = new LinkedList<>();
    }

    /**
     * Returns the published resource corresponding to the input element, if any.
     */
    public IdentifiedElement get(Object input) {
        return mappings.get(input);
    }

    /**
     * Associates a published resource to the input element.
     */
    public void put(Object input, IdentifiedElement output) {
        this.mappings.put(input, output);
    }

    /**
     * Registers a Runnable instance to execute later, through the createBuilder() method,
     * input elements have a corresponding published resources.
     */
    public void add(Runnable runnable) {
        this.runnables.add(runnable);
    }

    /**
     * Creates a build operation to execute the Runnable instances previously registered.
     */
    public Operation createBuilder() {
        return new Operation("Build Mappings") {

            @Override
            protected void run(IProgressMonitor monitor) {
                for (Iterator<Runnable> i = runnables.iterator(); i.hasNext();) {
                    Runnable runnable = i.next();
                    i.remove();

                    runnable.run();

                    if (isCanceledOrFailed(monitor)) {
                        return;
                    }
                }

            }
        };
    }
}
