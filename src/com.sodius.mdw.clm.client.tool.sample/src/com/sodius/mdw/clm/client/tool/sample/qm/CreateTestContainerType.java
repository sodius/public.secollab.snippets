package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the Type for a container of test resources.
 */
class CreateTestContainerType extends PublishOperation<Type> {

    private final Design design;

    CreateTestContainerType(Design design, PublishContext context) {
        super("Create Test Container Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forTestContainerType();
        Type type = design.createType(guid, "Test Container", "TestContainer");
        setResult(type);
    }
}
