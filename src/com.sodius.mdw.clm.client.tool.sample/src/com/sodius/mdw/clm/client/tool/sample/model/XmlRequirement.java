package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.List;

import com.sodius.mdw.metamodel.xml.Element;

public class XmlRequirement extends XmlElementWrapper {

    XmlRequirement(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getText() {
        return getElement().getAttribute("text");
    }

    public String getPriority() {
        return getElement().getAttribute("priority");
    }

    public Integer getProgress() {
        String value = getElement().getAttribute("progress");
        if (value == null) {
            return null;
        } else {
            return Integer.parseInt(value);
        }
    }

    public XmlSpecification getSpecification() {
        if ("specification".equals(getElement().getParent().getTagName())) {
            return new XmlSpecification(getElement().getParent());
        } else if ("requirement".equals(getElement().getParent().getTagName())) {
            return new XmlRequirement(getElement().getParent()).getSpecification();
        } else {
            return null;
        }
    }

    public XmlRequirement getParentRequirement() {
        if ("requirement".equals(getElement().getParent().getTagName())) {
            return new XmlRequirement(getElement().getParent());
        } else {
            return null;
        }
    }

    public List<XmlRequirement> getSubRequirements() {
        List<XmlRequirement> result = new ArrayList<>();
        for (Object content : getElement().getContent()) {
            if (content instanceof Element) {
                Element contentElement = (Element) content;
                if ("requirement".equals(contentElement.getTagName())) {
                    result.add(new XmlRequirement(contentElement));
                }
            }
        }
        return result;
    }

    public List<XmlElementWrapper> getSatisfies() {
        return getReferencedElements("satisfies");
    }

    public List<XmlElementWrapper> getSatisfiedBy() {
        return getReferencedElements("satisfiedBy");
    }

    public List<XmlElementWrapper> getImplementedBy() {
        return getReferencedElements("implementedBy");
    }

    public List<XmlElementWrapper> getElaboratedBy() {
        return getReferencedElements("elaboratedBy");
    }

}
