package com.sodius.mdw.clm.client.tool.sample.ui;

import com.sodius.mdw.clm.client.ui.AuthoringToolWizard;

/**
 * The wizard that displays pages specific to this publisher so that the user can provide input to publish.
 */
public class SampleWizard extends AuthoringToolWizard {

    /**
     * Provides only one page that allows selecting a file to publish.
     */
    @Override
    public void addPages() {
        addPage(new SampleWizardPage());
    }

}
