package com.sodius.mdw.clm.client.tool.sample.ui;

/**
 * The settings to share between wizard pages and the publish operation executing.
 */
public class SamplePublishSetting {

    /**
     * The file to publish, as selected in the wizard page.
     */
    public static final String SELECTED_FILE = "sample.fileLocation";

    private SamplePublishSetting() {
    }

}
