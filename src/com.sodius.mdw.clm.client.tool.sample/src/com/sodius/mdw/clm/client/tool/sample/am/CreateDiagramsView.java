package com.sodius.mdw.clm.client.tool.sample.am;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Diagram;
import com.sodius.mdw.clm.common.publish.ExplorerPage;
import com.sodius.mdw.clm.common.publish.ExplorerView;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Row;
import com.sodius.mdw.clm.common.publish.Table;
import com.sodius.mdw.clm.common.publish.Tree;
import com.sodius.mdw.clm.common.publish.TreeNode;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.clm.common.publish.XMLLiteralUtils;
import com.sodius.mdw.clm.common.publish.operations.TitleComparator;

/**
 * Creates an explorer view to display a tree of diagrams.
 *
 * <p>
 * The tree structure and behavior are the following:
 * <ul>
 * <li>
 * Root nodes are the types of diagrams.
 * Clicking a node shows a table that lists all diagrams of that type.
 * </li>
 * <li>
 * Child nodes are the diagram instances of a specific type.
 * Clicking a node shows the related diagram.
 * </li>
 * </ul>
 */
class CreateDiagramsView extends PublishOperation<ExplorerView> {

    private final Design design;
    private final ExplorerPage page;
    private final Media folderIcon;

    public CreateDiagramsView(Design design, ExplorerPage page, Media folderIcon, PublishContext context) {
        super("Create Diagrams View", context.getSettings(), context);
        this.design = design;
        this.page = page;
        this.folderIcon = folderIcon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // view
        ExplorerView view = page.createTreeView(Identifiers.forExplorerDiagramsView(), "Diagrams");
        Tree tree = (Tree) view.getToc();
        setResult(view);

        // diagram types
        for (Type type : getDiagramTypes()) {

            // diagram type tree node
            TreeNode typeNode = tree.createNode(type.getTitle());
            typeNode.setIcon(folderIcon);

            // table of diagrams
            Table table = createTable(type);
            typeNode.setTable(table);

            // sort diagrams
            List<Resource> diagrams = new ArrayList<>(type.getResources());
            Collections.sort(diagrams, new TitleComparator());

            // child nodes
            for (Resource diagram : diagrams) {

                // diagram tree node
                typeNode.createNode(diagram);

                // diagram table row
                Row row = table.createRow(diagram);
                row.createCell(getDiagramCellValue(diagram));
            }
        }
    }

    private Table createTable(Type type) {
        GUID guid = GUID.valueOf(getResult().getToc().getGuid()).append(type.getGuid());
        Table table = design.eModel().create(PublishPackage.Literals.TABLE);
        table.setGuid(guid.toString());
        table.createColumn("Diagrams");
        return table;
    }

    private List<Type> getDiagramTypes() {
        List<Type> result = new ArrayList<>();
        for (Type type : design.getTypes()) {
            if (!type.getResources().isEmpty() && (type.getResources().first() instanceof Diagram)) {
                result.add(type);
            }
        }
        Collections.sort(result, new TitleComparator());
        return result;
    }

    private String getDiagramCellValue(Resource resource) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(XMLLiteralUtils.HTML_BODY_START);
        buffer.append(XMLLiteralUtils.htmlImage(resource.getIcon()));
        buffer.append(XMLLiteralUtils.HTML_NON_BREAKING_SPACE);
        buffer.append(XMLLiteralUtils.toXMLLiteral(resource.getTitle()));
        buffer.append(XMLLiteralUtils.HTML_BODY_END);
        return buffer.toString();
    }
}
