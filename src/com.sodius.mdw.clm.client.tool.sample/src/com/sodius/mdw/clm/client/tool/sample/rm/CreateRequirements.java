package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRequirement;
import com.sodius.mdw.clm.client.tool.sample.model.XmlSpecification;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the requirements.
 */
class CreateRequirements extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    CreateRequirements(Model sourceModel, Design design, PublishContext context) {
        super("Create Requirements", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for requirements
        Media icon = MediaFactory.getInstance(getPublishContext()).createRequirementIcon();
        Type type = run(new CreateRequirementType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each Requirement, going in depth from the specification roots
        for (XmlSpecification specification : XmlSpecification.getInstances(sourceModel)) {
            for (XmlRequirement requirement : specification.getRootRequirements()) {
                createRequirementHierarchy(requirement, type, icon, monitor);
                if (isCanceledOrFailed(monitor)) {
                    return;
                }
            }
        }

        // Step 3 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }

    /*
     * Creates a Requirement and all nested requirements.
     */
    private void createRequirementHierarchy(XmlRequirement requirement, Type type, Media requirementIcon, IProgressMonitor monitor) {

        // create requirement resource
        run(new CreateRequirement(requirement, type, requirementIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // create nested requirements
        for (XmlRequirement subRequirement : requirement.getSubRequirements()) {
            createRequirementHierarchy(subRequirement, type, requirementIcon, monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }
    }
}
