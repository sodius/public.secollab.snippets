package com.sodius.mdw.clm.client.tool.sample;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagram;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.operations.PublishURIConverter;

/**
 * A factory to create Media instances.
 */
public class MediaFactory {

    /*
     * Obtain the shared factory for the specified context.
     */
    public static MediaFactory getInstance(PublishContext context) {
        return (MediaFactory) context.getProperties().getProperty(MediaFactory.class.getName());
    }

    /*
     * Creates a factory once the Design instance is known.
     */
    static MediaFactory createInstance(PublishContext context, Design design) {

        // create instance
        MediaFactory factory = new MediaFactory(context, design);

        // register it in the OperationContext, so that is is shared and reusable in getInstance(PublishContext)
        context.getProperties().setProperty(MediaFactory.class.getName(), factory);

        return factory;
    }

    private final PublishContext context;
    private final Design design;

    private MediaFactory(PublishContext context, Design design) {
        this.context = context;
        this.design = design;
    }

    public Media createToolIcon() {
        GUID guid = Identifiers.forToolIcon();
        return createPluginIcon(guid, "Tool.gif");
    }

    public Media createFolderIcon() {
        GUID guid = Identifiers.forFolderIcon();
        return createPluginIcon(guid, "Folder.gif");
    }

    public Media createSpecificationIcon() {
        GUID guid = Identifiers.forSpecificationIcon();
        return createPluginIcon(guid, "Specification.png");
    }

    public Media createRequirementIcon() {
        GUID guid = Identifiers.forRequirementIcon();
        return createPluginIcon(guid, "Requirement.gif");
    }

    public Media createChangeRequestContainerIcon() {
        GUID guid = Identifiers.forChangeRequestContainerIcon();
        return createPluginIcon(guid, "ChangeRequests.png");
    }

    public Media createChangeRequestIcon(String type) {
        GUID guid = Identifiers.forChangeRequestIcon(type);
        return createPluginIcon(guid, type + ".png");
    }

    public Media createTestContainerIcon() {
        GUID guid = Identifiers.forTestContainerIcon();
        return createPluginIcon(guid, "Tests.png");
    }

    public Media createTestPlanIcon() {
        GUID guid = Identifiers.forTestPlanIcon();
        return createPluginIcon(guid, "TestPlan.png");
    }

    public Media createTestCaseIcon() {
        GUID guid = Identifiers.forTestCaseIcon();
        return createPluginIcon(guid, "TestCase.png");
    }

    public Media createTestScriptIcon() {
        GUID guid = Identifiers.forTestScriptIcon();
        return createPluginIcon(guid, "TestScript.png");
    }

    public Media createTestExecutionRecordIcon() {
        GUID guid = Identifiers.forTestExecutionRecordIcon();
        return createPluginIcon(guid, "TestExecutionRecord.png");
    }

    public Media createTestResultIcon() {
        GUID guid = Identifiers.forTestResultIcon();
        return createPluginIcon(guid, "TestResult.png");
    }

    public Media createReleaseIcon() {
        GUID guid = Identifiers.forReleaseIcon();
        return createPluginIcon(guid, "Release.png");
    }

    public Media createPackageIcon() {
        GUID guid = Identifiers.forPackageIcon();
        return createPluginIcon(guid, "Package.gif");
    }

    public Media createItemIcon(String type) {
        GUID guid = Identifiers.forItemIcon(type);
        return createPluginIcon(guid, type + ".gif");
    }

    public Media createDiagramIcon() {
        GUID guid = Identifiers.forDiagramIcon();
        return createPluginIcon(guid, "Diagram.gif");
    }

    public Media createDiagramImage(XmlDiagram diagram) {

        // determine the location of the XML file being loaded
        File xmlFilename = new File(diagram.getElement().eModel().getReadURI());

        // expect to find a PNG image in that directory based on the diagram ID
        File diagramFile = new File(xmlFilename.getParentFile(), diagram.getId() + ".png");
        if (!diagramFile.exists()) {
            throw new RuntimeException("Diagram image does not exist: " + diagramFile);
        }

        // create media
        GUID guid = Identifiers.forDiagramImage(diagram);
        Media media = design.createMedia(guid, diagramFile.getName(), "image/png");

        // associate the file URI to the media resource (i.e. where to read the byte stream)
        setMediaLocation(media, URI.createFileURI(diagramFile.getPath()));

        return media;
    }

    /*
     * Creates a Media instance for the specified icon filename, expected to be in the /icons/obj16/ folder of this plug-in.
     */
    private Media createPluginIcon(GUID guid, String filename) {

        // create media
        String mimeType = getMimeType(filename);
        Media media = design.createMedia(guid, filename, mimeType);

        // determines what plug-in resource corresponds to the media resource (i.e. where to read the byte stream)
        String pluginResourcePath = "/icons/obj16/" + filename;
        URI pluginResourceUri = PublishURIConverter.createPluginIcon(Activator.getContext().getBundle(), pluginResourcePath);
        setMediaLocation(media, pluginResourceUri);

        return media;
    }

    private void setMediaLocation(Media media, URI uri) {
        PublishURIConverter converter = (PublishURIConverter) context.getProperties().getProperty(URIConverter.class.getName());
        converter.addMapping(media.getUri(), uri);
    }

    private String getMimeType(String filename) {
        if (filename.endsWith(".gif")) {
            return "image/gif";
        } else {
            return "image/png";
        }
    }

}
