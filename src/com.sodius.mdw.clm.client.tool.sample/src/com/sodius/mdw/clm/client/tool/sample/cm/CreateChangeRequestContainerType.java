package com.sodius.mdw.clm.client.tool.sample.cm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the Type for a container of Change Requests.
 */
class CreateChangeRequestContainerType extends PublishOperation<Type> {

    private final Design design;

    CreateChangeRequestContainerType(Design design, PublishContext context) {
        super("Create Issue Container Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forChangeRequestContainerType();
        Type type = design.createType(guid, "Issue Container", "IssueContainer");
        setResult(type);
    }
}
