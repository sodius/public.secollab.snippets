package com.sodius.mdw.clm.client.tool.sample.am;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Diagram;
import com.sodius.mdw.clm.common.publish.ExplorerPage;
import com.sodius.mdw.clm.common.publish.ExplorerView;
import com.sodius.mdw.clm.common.publish.ListNode;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Row;
import com.sodius.mdw.clm.common.publish.Table;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.clm.common.publish.XMLLiteralUtils;
import com.sodius.mdw.clm.common.publish.operations.TitleComparator;

/**
 * Creates an explorer view to display a list of resource types.
 *
 * <p>
 * The view contains a node for each type of resource.
 * Clicking a node shows a table that lists all resources of that type.
 */
public class CreateTypesView extends PublishOperation<ExplorerView> {

    private final Design design;
    private final ExplorerPage page;

    public CreateTypesView(Design design, ExplorerPage page, PublishContext context) {
        super("Create Types View", context.getSettings(), context); //$NON-NLS-1$
        this.design = design;
        this.page = page;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // view
        ExplorerView view = page.createListView(Identifiers.forExplorerTypesView(), "Types");
        com.sodius.mdw.clm.common.publish.List list = (com.sodius.mdw.clm.common.publish.List) view.getToc();
        setResult(view);

        // object types
        for (Type type : getResourceTypes()) {

            // object type tree node
            ListNode typeNode = list.createNode(type.getTitle());

            // table of objects
            typeNode.setTable(createTable(type));
        }
    }

    private Table createTable(Type type) {

        // table of objects
        GUID guid = GUID.valueOf(getResult().getToc().getGuid()).append(type.getGuid());
        Table table = design.eModel().create(PublishPackage.Literals.TABLE);
        table.setGuid(guid.toString());
        table.createColumn("Resources");

        // sort objects
        List<Resource> resources = new ArrayList<>(type.getResources());
        Collections.sort(resources, new TitleComparator());

        // child nodes
        for (Resource resource : resources) {

            // object table row
            Row row = table.createRow(resource);
            row.createCell(getResourceCellValue(resource));
        }

        return table;
    }

    private List<Type> getResourceTypes() {
        List<Type> result = new ArrayList<>();
        for (Type type : design.getTypes()) {
            if (!type.getResources().isEmpty()) {
                Resource firstResource = type.getResources().first();
                if (!(firstResource instanceof Diagram) && !(design.getDocuments().contains(firstResource))) {
                    result.add(type);
                }
            }
        }
        Collections.sort(result, new TitleComparator());
        return result;
    }

    private String getResourceCellValue(Resource resource) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(XMLLiteralUtils.HTML_BODY_START);
        buffer.append(XMLLiteralUtils.htmlImage(resource.getIcon()));
        buffer.append(XMLLiteralUtils.HTML_NON_BREAKING_SPACE);
        buffer.append(XMLLiteralUtils.toXMLLiteral(resource.getTitle()));
        buffer.append(XMLLiteralUtils.HTML_BODY_END);
        return buffer.toString();
    }
}
