package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestExecutionRecord;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.TestExecutionRecord;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a test execution record
 */
class CreateTestExecutionRecord extends AbstractCreateResource<TestExecutionRecord> {

    private final XmlTestExecutionRecord testExecutionRecord;

    CreateTestExecutionRecord(XmlTestExecutionRecord testExecutionRecord, Type type, Media icon, PublishContext context) {
        super("Create Test Execution Record: " + testExecutionRecord.getId(), type, icon, context);
        this.testExecutionRecord = testExecutionRecord;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Test ExecutionRecord
        GUID guid = Identifiers.forTestExecutionRecordResource(testExecutionRecord);
        TestExecutionRecord resource = getType().createTestExecutionRecord(guid, testExecutionRecord.getName());
        resource.setShortTitle(testExecutionRecord.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(testExecutionRecord, resource);

        // create links
        createLink(resource, CreateTestExecutionRecordType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
    }

}
