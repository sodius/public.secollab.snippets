package com.sodius.mdw.clm.client.tool.sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.common.publish.OSLCProperty;
import com.sodius.mdw.clm.common.publish.PrimitiveProperty;
import com.sodius.mdw.clm.common.publish.PropertiesGroup;
import com.sodius.mdw.clm.common.publish.Property;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.clm.common.publish.operations.TitleComparator;

/**
 * Create a property view for a Type.
 *
 * A property view is used for displaying the details of a resource.
 * It is also used for displaying an overview (a.k.a preview) of a resource when the user's mouse lingers over the link to the resource.
 *
 * In this sample, the same property view is used for both full detailed view and preview.
 * In a real world publisher, it is recommended to have separate property views,
 * as the preview generally displays less attributes than the full detailed view.
 */
public class CreatePropertyView extends PublishOperation<PropertyView> {

    private final Type type;

    public CreatePropertyView(Type type, PublishContext context) {
        super("Create View for " + type.getTitle(), context.getSettings(), context);
        this.type = type;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create view
        PropertyView view = type.getDesign().createPropertyView();
        setResult(view);

        // create Standard group
        PropertiesGroup group = view.createGroup("Standard Attributes");

        // show built-in OSLC properties
        group.createProperty(OSLCProperty.TITLE);
        group.createProperty(OSLCProperty.TYPE);
        group.createProperty(OSLCProperty.CREATED);
        group.createProperty(OSLCProperty.MODIFIED);

        // show primitive properties defined on the type, in alphabetical order
        List<Property> properties = new ArrayList<>(type.getProperties());
        Collections.sort(properties, new TitleComparator());
        for (Property property : properties) {
            if (property instanceof PrimitiveProperty) {
                group.createProperty((PrimitiveProperty) property);
            }
        }
    }
}
