package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlTestExecutionRecord extends XmlElementWrapper {

    public static Collection<XmlTestExecutionRecord> getInstances(Model model) {
        Collection<XmlTestExecutionRecord> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("testExecutionRecord".equals(element.getTagName())) {
                result.add(new XmlTestExecutionRecord(element));
            }
        }
        return result;
    }

    XmlTestExecutionRecord(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

}
