package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.PrimitiveType;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the type for test result.
 */
class CreateTestResultType extends PublishOperation<Type> {

    static final String PROPERTY_STATUS = "Status";
    static final String PROPERTY_CONTAINER = "Container";
    static final String PROPERTY_REPORTS_ON = "Reports On";
    static final String PROPERTY_EXECUTES = "Executes";
    static final String PROPERTY_RUNS_ON = "Runs On";

    private final Design design;

    CreateTestResultType(Design design, PublishContext context) {
        super("Create Test Result Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forTestResultType();
        Type type = design.createType(guid, "Test Result", "TestResult");
        setResult(type);

        // create properties
        type.createPrimitiveProperty(PROPERTY_STATUS, PrimitiveType.STRING);
        type.createResourceProperty(PROPERTY_CONTAINER).setContainer(true);
        type.createResourcesProperty(PROPERTY_REPORTS_ON);
        type.createResourcesProperty(PROPERTY_EXECUTES);
        type.createResourcesProperty(PROPERTY_RUNS_ON);
    }
}
