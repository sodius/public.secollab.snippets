package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRelease;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.GenericResource;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a release
 */
class CreateRelease extends AbstractCreateResource<GenericResource> {

    private final XmlRelease release;

    CreateRelease(XmlRelease release, Type type, Media icon, PublishContext context) {
        super("Create Release: " + release.getId(), type, icon, context);
        this.release = release;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Release
        GUID guid = Identifiers.forReleaseResource(release);
        GenericResource resource = getType().createGenericResource(guid, release.getName());
        resource.setShortTitle(release.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(release, resource);

        // create links
        createLink(resource, CreateReleaseType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateReleaseType.PROPERTY_VALIDATED_BY, release.getValidatedBy());
    }

}
