package com.sodius.mdw.clm.client.tool.sample.rm;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRequirement;
import com.sodius.mdw.clm.client.tool.sample.model.XmlSpecification;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Row;
import com.sodius.mdw.clm.common.publish.Table;
import com.sodius.mdw.clm.common.publish.TablePage;
import com.sodius.mdw.clm.common.publish.TableView;

/**
 * Create a table page showing all Requirements of a Specification.
 */
class CreateSpecificationPage extends PublishOperation<TablePage> {

    private final XmlSpecification specification;

    CreateSpecificationPage(XmlSpecification specification, PublishContext context) {
        super("Create Page for " + specification.getId(), context.getSettings(), context);
        this.specification = specification;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create page
        Resource resource = (Resource) Mapping.getInstance(getPublishContext()).get(specification);
        TablePage page = resource.eModel().create(PublishPackage.Literals.TABLE_PAGE);
        resource.setPage(page);
        setResult(page);

        // create table
        GUID guid = Identifiers.forSpecificationTable(specification);
        TableView tableView = page.createView(guid, "Requirements");
        Table table = tableView.getTable();

        // create columns
        table.createColumn("ID").setHintWidth("120px");
        table.createColumn("Text");
        table.createColumn("Priority").setHintWidth("100px");

        // create rows
        createRows(table, specification.getRootRequirements());

        // use the same table for document reporting on this specification
        page.setReportView(tableView);
    }

    private void createRows(Table table, List<XmlRequirement> rootRequirements) {

        // loop on requirements
        for (XmlRequirement requirement : rootRequirements) {
            Resource resource = (Resource) Mapping.getInstance(getPublishContext()).get(requirement);

            // create row
            Row row = table.createRow(resource);
            row.createCell(requirement.getId());
            row.createCell(requirement.getText());
            row.createCell(requirement.getPriority());

            // loop on children
            createRows(table, requirement.getSubRequirements());
        }

    }
}
