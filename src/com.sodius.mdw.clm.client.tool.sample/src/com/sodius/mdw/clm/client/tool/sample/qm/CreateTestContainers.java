package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Resource;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the containers of Change Requests.
 */
public class CreateTestContainers extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;
    private final Media folderIcon;

    public CreateTestContainers(Model sourceModel, Design design, Media folderIcon, PublishContext context) {
        super("Create Test Containers", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
        this.folderIcon = folderIcon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for test container
        Media containerIcon = MediaFactory.getInstance(getPublishContext()).createTestContainerIcon();
        Type type = run(new CreateTestContainerType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for the test container
        Resource container = run(new CreateTestContainer(type, containerIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 3 - create all test artifacts
        run(new CreateTestPlans(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
        run(new CreateTestCases(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
        run(new CreateTestScripts(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
        run(new CreateTestExecutionRecords(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
        run(new CreateTestResults(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
        run(new CreateReleases(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 4 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);

        // Step 5 - create pages
        run(new CreateTestContainerPage(container, folderIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
    }
}
