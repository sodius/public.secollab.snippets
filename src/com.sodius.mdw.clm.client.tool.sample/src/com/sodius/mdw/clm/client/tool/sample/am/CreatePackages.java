package com.sodius.mdw.clm.client.tool.sample.am;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.common.publish.AMResource;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the architecture resources, view and pages
 */
public class CreatePackages extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;
    private final Media folderIcon;

    public CreatePackages(Model sourceModel, Design design, Media folderIcon, PublishContext context) {
        super("Create Package", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
        this.folderIcon = folderIcon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for AM Package
        Media packageIcon = MediaFactory.getInstance(getPublishContext()).createPackageIcon();
        Type type = run(new CreatePackageType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for AM Package
        AMResource owningPackage = run(new CreatePackage(type, packageIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 3 - create all AM resources
        run(new CreateItems(sourceModel, design, owningPackage, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 4 - create all Diagrams
        run(new CreateDiagrams(sourceModel, design, owningPackage, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 5 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);

        // Step 6 - create pages
        run(new CreatePackagePage(owningPackage, folderIcon, getPublishContext()), monitor);
    }
}
