package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlElementWrapper {

    private final Element element;

    XmlElementWrapper(Element element) {
        this.element = element;
    }

    public final Element getElement() {
        return element;
    }

    @Override
    public final int hashCode() {
        return element.hashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof XmlElementWrapper) {
            return element.equals(((XmlElementWrapper) obj).element);
        } else {
            return false;
        }
    }

    final List<XmlElementWrapper> getReferencedElements(String referenceTagName) {

        // build id/element map
        Map<String, XmlElementWrapper> allElements = new HashMap<>();
        for (Element xmlElement : getElement().eModel().<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            String id = xmlElement.getAttribute("id");
            if (id != null) {
                allElements.put(id, new XmlElementWrapper(xmlElement));
            }
        }

        // loop on nested elements
        List<XmlElementWrapper> result = new ArrayList<>();
        for (Object content : getElement().getContent()) {
            if (content instanceof Element) {
                Element contentElement = (Element) content;

                // match the expected reference type?
                if (referenceTagName.equals(contentElement.getTagName())) {

                    // lookup referenced requirement
                    String idref = contentElement.getAttribute("idref");
                    XmlElementWrapper referencedElement = allElements.get(idref);
                    if (referencedElement == null) {
                        throw new RuntimeException("No such element: " + idref);
                    } else {
                        result.add(referencedElement);
                    }
                }
            }
        }
        return result;
    }

    final XmlElementWrapper getReferencedAttribute(String attributeName) {

        // get attribute value
        String idref = getElement().getAttribute(attributeName);
        if (idref == null) {
            return null;
        } else {

            // loop on all elements
            for (Element xmlElement : getElement().eModel().<Element> getInstances(XmlPackage.Literals.ELEMENT)) {

                // matches the expected identifier?
                String id = xmlElement.getAttribute("id");
                if (idref.equals(id)) {
                    return new XmlElementWrapper(xmlElement);
                }
            }

            throw new RuntimeException("No such element: " + idref);
        }
    }
}
