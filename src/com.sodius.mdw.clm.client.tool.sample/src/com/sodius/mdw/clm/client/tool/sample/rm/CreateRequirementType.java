package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.PrimitiveType;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the requirements.
 */
class CreateRequirementType extends PublishOperation<Type> {

    static final String PROPERTY_PRIORITY = "Priority";
    static final String PROPERTY_PROGRESS = "Progress";
    static final String PROPERTY_SPECIFICATION = "Specification";
    static final String PROPERTY_PARENT = "Parent";
    static final String PROPERTY_SUB_REQUIREMENTS = "Sub-Requirements";
    static final String PROPERTY_SATISFIES = "Satisfies";
    static final String PROPERTY_SATISFIED_BY = "Satisfied By";
    static final String PROPERTY_ELABORATED_BY = "Elaborated By";
    static final String PROPERTY_IMPLEMENTED_BY = "Implemented By";

    private final Design design;

    CreateRequirementType(Design design, PublishContext context) {
        super("Create Requirement Type", context.getSettings(), context);
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create type
        GUID guid = Identifiers.forRequirementType();
        Type type = design.createType(guid, "Requirement", "Requirement");
        setResult(type);

        // create properties
        type.createPrimitiveProperty(PROPERTY_PRIORITY, PrimitiveType.STRING);
        type.createPrimitiveProperty(PROPERTY_PROGRESS, PrimitiveType.INT);
        type.createResourceProperty(PROPERTY_SPECIFICATION).setContainer(true);
        type.createResourceProperty(PROPERTY_PARENT);
        type.createResourcesProperty(PROPERTY_SUB_REQUIREMENTS).setOrdered(true);
        type.createResourcesProperty(PROPERTY_SATISFIES);
        type.createResourcesProperty(PROPERTY_SATISFIED_BY);
        type.createResourcesProperty(PROPERTY_ELABORATED_BY);
        type.createResourcesProperty(PROPERTY_IMPLEMENTED_BY);
    }
}
