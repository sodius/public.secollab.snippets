package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestPlan;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the test plans.
 */
class CreateTestPlans extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    CreateTestPlans(Model sourceModel, Design design, PublishContext context) {
        super("Create Test Plans", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for test plans
        Media icon = MediaFactory.getInstance(getPublishContext()).createTestPlanIcon();
        Type type = run(new CreateTestPlanType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each Test Plan
        for (XmlTestPlan testPlan : XmlTestPlan.getInstances(sourceModel)) {
            run(new CreateTestPlan(testPlan, type, icon, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 3 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }
}
