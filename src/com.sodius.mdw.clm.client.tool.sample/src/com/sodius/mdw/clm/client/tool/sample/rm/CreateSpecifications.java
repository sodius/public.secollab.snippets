package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlSpecification;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the specification documents, their contained resources and pages.
 */
public class CreateSpecifications extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    public CreateSpecifications(Model sourceModel, Design design, PublishContext context) {
        super("Create Specifications", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for requirement specifications
        Media icon = MediaFactory.getInstance(getPublishContext()).createSpecificationIcon();
        Type type = run(new CreateSpecificationType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each specification
        for (XmlSpecification specification : XmlSpecification.getInstances(sourceModel)) {
            run(new CreateSpecification(specification, type, icon, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 3 - create all Requirements
        run(new CreateRequirements(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 4 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);

        // Step 5 - create pages
        for (XmlSpecification specification : XmlSpecification.getInstances(sourceModel)) {
            run(new CreateSpecificationPage(specification, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }
    }
}
