package com.sodius.mdw.clm.client.tool.sample.rm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRequirement;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Requirement;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a requirement.
 */
class CreateRequirement extends AbstractCreateResource<Requirement> {

    private final XmlRequirement requirement;

    CreateRequirement(XmlRequirement requirement, Type type, Media icon, PublishContext context) {
        super("Create Requirement: " + requirement.getId(), type, icon, context);
        this.requirement = requirement;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Requirement
        GUID guid = Identifiers.forRequirementResource(requirement);
        Requirement resource = getType().createRequirement(guid, requirement.getText());
        resource.setShortTitle(requirement.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(requirement, resource);

        // set attribute values
        String priority = requirement.getPriority();
        if (priority != null) {
            resource.createValue(getType().getProperty(CreateRequirementType.PROPERTY_PRIORITY), priority);
        }

        Integer progress = requirement.getProgress();
        if (progress != null) {
            resource.createValue(getType().getProperty(CreateRequirementType.PROPERTY_PROGRESS), progress);
        }

        // create links
        createLink(resource, CreateRequirementType.PROPERTY_SPECIFICATION, requirement.getSpecification());
        createLink(resource, CreateRequirementType.PROPERTY_PARENT, requirement.getParentRequirement());
        createLinks(resource, CreateRequirementType.PROPERTY_SUB_REQUIREMENTS, requirement.getSubRequirements());
        createLinks(resource, CreateRequirementType.PROPERTY_SATISFIES, requirement.getSatisfies());
        createLinks(resource, CreateRequirementType.PROPERTY_SATISFIED_BY, requirement.getSatisfiedBy());
        createLinks(resource, CreateRequirementType.PROPERTY_ELABORATED_BY, requirement.getElaboratedBy());
        createLinks(resource, CreateRequirementType.PROPERTY_IMPLEMENTED_BY, requirement.getImplementedBy());
    }

}
