package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlChangeRequest extends XmlElementWrapper {

    public static Collection<XmlChangeRequest> getInstances(Model model) {
        Collection<XmlChangeRequest> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("issue".equals(element.getTagName())) {
                result.add(new XmlChangeRequest(element));
            }
        }
        return result;
    }

    XmlChangeRequest(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public String getType() {
        return getElement().getAttribute("type");
    }

    public String getPriority() {
        return getElement().getAttribute("priority");
    }

    public String getStatus() {
        return getElement().getAttribute("status");
    }

    public List<XmlElementWrapper> getCauses() {
        return getReferencedElements("causes");
    }

    public List<XmlElementWrapper> getCausedBy() {
        return getReferencedElements("causedBy");
    }

    public List<XmlElementWrapper> getBlocks() {
        return getReferencedElements("blocks");
    }

    public List<XmlElementWrapper> getBlockedBy() {
        return getReferencedElements("blockedBy");
    }

}
