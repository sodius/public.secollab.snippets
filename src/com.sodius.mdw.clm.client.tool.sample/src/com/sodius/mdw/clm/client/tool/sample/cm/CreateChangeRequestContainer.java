package com.sodius.mdw.clm.client.tool.sample.cm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.GenericResource;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create the container for change requests.
 */
class CreateChangeRequestContainer extends PublishOperation<GenericResource> {
    static final Object CONTAINER_INPUT = new Object(); // just for using this in a Mapping for resolving links

    private final Type type;
    private final Media icon;

    CreateChangeRequestContainer(Type type, Media icon, PublishContext context) {
        super("Create Issue Container", context.getSettings(), context);
        this.type = type;
        this.icon = icon;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a container
        GUID guid = Identifiers.forChangeRequestContainerResource();
        GenericResource resource = type.createGenericResource(guid, "Issues");
        resource.setIcon(icon);
        setResult(resource);

        // register it as a document of the design
        type.getDesign().getDocuments().add(resource);

        // trace the mapping between the input and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(CONTAINER_INPUT, resource);
    }
}
