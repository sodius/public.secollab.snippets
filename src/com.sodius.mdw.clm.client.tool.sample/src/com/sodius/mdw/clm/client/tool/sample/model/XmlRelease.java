package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlRelease extends XmlElementWrapper {

    public static Collection<XmlRelease> getInstances(Model model) {
        Collection<XmlRelease> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("release".equals(element.getTagName())) {
                result.add(new XmlRelease(element));
            }
        }
        return result;
    }

    XmlRelease(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public List<XmlElementWrapper> getValidatedBy() {
        return getReferencedElements("validatedBy");
    }

}
