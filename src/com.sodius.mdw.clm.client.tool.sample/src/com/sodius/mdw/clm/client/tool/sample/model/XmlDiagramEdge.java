package com.sodius.mdw.clm.client.tool.sample.model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.sodius.mdw.metamodel.xml.Element;

public class XmlDiagramEdge extends XmlElementWrapper {

    XmlDiagramEdge(Element element) {
        super(element);
    }

    public XmlElementWrapper getItem() {
        return getReferencedAttribute("idref");
    }

    public List<Point> getPoints() {
        List<Point> result = new ArrayList<>();
        String[] points = getElement().getAttribute("points").split(";");
        for (String point : points) {
            int separatorIndex = point.indexOf(',');
            int x = Integer.parseInt(point.substring(0, separatorIndex));
            int y = Integer.parseInt(point.substring(separatorIndex + 1));
            result.add(new Point(x, y));
        }
        return result;
    }
}
