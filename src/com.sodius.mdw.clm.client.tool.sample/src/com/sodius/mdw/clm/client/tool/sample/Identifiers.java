package com.sodius.mdw.clm.client.tool.sample;

import com.sodius.mdw.clm.client.tool.sample.model.XmlChangeRequest;
import com.sodius.mdw.clm.client.tool.sample.model.XmlDiagram;
import com.sodius.mdw.clm.client.tool.sample.model.XmlItem;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRelease;
import com.sodius.mdw.clm.client.tool.sample.model.XmlRequirement;
import com.sodius.mdw.clm.client.tool.sample.model.XmlSpecification;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestCase;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestExecutionRecord;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestPlan;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestResult;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestScript;
import com.sodius.mdw.clm.common.model.GUID;

/**
 * Central place to obtain GUIDs for resources.
 *
 * A GUID is an identifier unique in the scope of the published design.
 * The GUID is used when publishing a new version of the same design,
 * to determine the resources that were changed and the ones that are unmodified.
 * Once assigned to a resource, a GUID must never change.
 *
 * GUIDs computed here are typically based on the XML "id" attribute, which is expected to never change.
 * If for example a requirement text or specification name is changed in a future revision of the sample data,
 * the GUID is left unmodified.
 * It enables SECollab to identify the previous version of a requirement and report a changed attribute,
 * rather than reporting a deleted requirement and a create one.
 */
public class Identifiers {

    public static GUID forToolIcon() {
        return GUID.create("tool", "icon");
    }

    public static GUID forFolderIcon() {
        return GUID.create("folder", "icon");
    }

    public static GUID forSpecificationType() {
        return GUID.create("specification", "type");
    }

    public static GUID forSpecificationIcon() {
        return GUID.create("specification", "icon");
    }

    public static GUID forSpecificationTable(XmlSpecification specification) {
        return GUID.create("specification", "table", specification.getId());
    }

    public static GUID forSpecificationResource(XmlSpecification specification) {
        return GUID.create("specification", "resource", specification.getId());
    }

    public static GUID forRequirementType() {
        return GUID.create("requirement", "type");
    }

    public static GUID forRequirementIcon() {
        return GUID.create("requirement", "icon");
    }

    public static GUID forRequirementResource(XmlRequirement requirement) {
        return GUID.create("requirement", "resource", requirement.getId());
    }

    public static GUID forChangeRequestContainerType() {
        return GUID.create("changeRequests", "type");
    }

    public static GUID forChangeRequestContainerIcon() {
        return GUID.create("changeRequests", "icon");
    }

    public static GUID forChangeRequestContainerTable() {
        return GUID.create("changeRequests", "table");
    }

    public static GUID forChangeRequestContainerResource() {
        return GUID.create("changeRequests", "container");
    }

    public static GUID forChangeRequestType() {
        return GUID.create("changeRequest", "type");
    }

    public static GUID forChangeRequestResource(XmlChangeRequest changeRequest) {
        return GUID.create("changeRequest", "resource", changeRequest.getId());
    }

    public static GUID forChangeRequestIcon(String name) {
        return GUID.create("changeRequest", name, "icon");
    }

    public static GUID forTestContainerType() {
        return GUID.create("tests", "type");
    }

    public static GUID forTestContainerIcon() {
        return GUID.create("tests", "icon");
    }

    public static GUID forTestContainerResource() {
        return GUID.create("tests", "container");
    }

    public static GUID forExplorerTestsView() {
        return GUID.create("explorer", "Tests");
    }

    public static GUID forTestPlanType() {
        return GUID.create("testPlan", "type");
    }

    public static GUID forTestPlanResource(XmlTestPlan testPlan) {
        return GUID.create("testPlan", "resource", testPlan.getId());
    }

    public static GUID forTestPlanIcon() {
        return GUID.create("testPlan", "icon");
    }

    public static GUID forTestCaseType() {
        return GUID.create("testCase", "type");
    }

    public static GUID forTestCaseResource(XmlTestCase testCase) {
        return GUID.create("testCase", "resource", testCase.getId());
    }

    public static GUID forTestCaseIcon() {
        return GUID.create("testCase", "icon");
    }

    public static GUID forTestScriptType() {
        return GUID.create("testScript", "type");
    }

    public static GUID forTestScriptResource(XmlTestScript testScript) {
        return GUID.create("testScript", "resource", testScript.getId());
    }

    public static GUID forTestScriptIcon() {
        return GUID.create("testScript", "icon");
    }

    public static GUID forTestExecutionRecordType() {
        return GUID.create("testExecutionRecord", "type");
    }

    public static GUID forTestExecutionRecordResource(XmlTestExecutionRecord testExecutionRecord) {
        return GUID.create("testExecutionRecord", "resource", testExecutionRecord.getId());
    }

    public static GUID forTestExecutionRecordIcon() {
        return GUID.create("testExecutionRecord", "icon");
    }

    public static GUID forTestResultType() {
        return GUID.create("testResult", "type");
    }

    public static GUID forTestResultResource(XmlTestResult testResult) {
        return GUID.create("testResult", "resource", testResult.getId());
    }

    public static GUID forTestResultIcon() {
        return GUID.create("testResult", "icon");
    }

    public static GUID forReleaseType() {
        return GUID.create("release", "type");
    }

    public static GUID forReleaseResource(XmlRelease release) {
        return GUID.create("release", "resource", release.getId());
    }

    public static GUID forReleaseIcon() {
        return GUID.create("release", "icon");
    }

    public static GUID forPackageType() {
        return GUID.create("package", "type");
    }

    public static GUID forPackageIcon() {
        return GUID.create("package", "icon");
    }

    public static GUID forPackageResource(String name) {
        return GUID.create("package", "resource", name);
    }

    public static GUID forItemType(String name) {
        return GUID.create("item", name, "type");
    }

    public static GUID forItemIcon(String name) {
        return GUID.create("item", name, "icon");
    }

    public static GUID forItemResource(XmlItem item) {
        return GUID.create("item", "resource", item.getId());
    }

    public static GUID forDiagramType(String name) {
        return GUID.create("diagram", name, "type");
    }

    public static GUID forDiagramIcon() {
        return GUID.create("diagram", "icon");
    }

    public static GUID forDiagramResource(XmlDiagram item) {
        return GUID.create("diagram", "resource", item.getId());
    }

    public static GUID forExplorerDiagramsView() {
        return GUID.create("explorer", "diagrams");
    }

    public static GUID forExplorerTypesView() {
        return GUID.create("explorer", "types");
    }

    public static GUID forDiagramImage(XmlDiagram item) {
        // append an "image" fragment to the GUID of the item
        return forDiagramResource(item).append("image");
    }

    private Identifiers() {
    }

}
