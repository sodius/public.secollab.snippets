package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestCase;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the test cases.
 */
class CreateTestCases extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    CreateTestCases(Model sourceModel, Design design, PublishContext context) {
        super("Create Test Cases", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for test cases
        Media icon = MediaFactory.getInstance(getPublishContext()).createTestCaseIcon();
        Type type = run(new CreateTestCaseType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for each Test Case
        for (XmlTestCase testCase : XmlTestCase.getInstances(sourceModel)) {
            run(new CreateTestCase(testCase, type, icon, getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }
        }

        // Step 3 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);
    }
}
