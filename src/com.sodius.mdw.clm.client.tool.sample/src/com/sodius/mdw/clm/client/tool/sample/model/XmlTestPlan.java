package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlTestPlan extends XmlElementWrapper {

    public static Collection<XmlTestPlan> getInstances(Model model) {
        Collection<XmlTestPlan> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("testPlan".equals(element.getTagName())) {
                result.add(new XmlTestPlan(element));
            }
        }
        return result;
    }

    XmlTestPlan(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public List<XmlElementWrapper> getValidates() {
        return getReferencedElements("validates");
    }

    public List<XmlElementWrapper> getReleases() {
        return getReferencedElements("releases");
    }

    public List<XmlElementWrapper> getUses() {
        return getReferencedElements("uses");
    }

}
