package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlTestResult extends XmlElementWrapper {

    public static Collection<XmlTestResult> getInstances(Model model) {
        Collection<XmlTestResult> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("testResult".equals(element.getTagName())) {
                result.add(new XmlTestResult(element));
            }
        }
        return result;
    }

    XmlTestResult(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public String getStatus() {
        return getElement().getAttribute("status");
    }

    public List<XmlElementWrapper> getReportsOn() {
        return getReferencedElements("reportsOn");
    }

    public List<XmlElementWrapper> getExecutes() {
        return getReferencedElements("executes");
    }

    public List<XmlElementWrapper> getRunsOn() {
        return getReferencedElements("runsOn");
    }

}
