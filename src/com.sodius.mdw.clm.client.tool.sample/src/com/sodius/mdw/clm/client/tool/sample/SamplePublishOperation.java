package com.sodius.mdw.clm.client.tool.sample;

import java.io.File;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.jface.dialogs.IDialogSettings;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.SaveModelOperation;
import com.sodius.mdw.clm.common.publish.PublishPackage;
import com.sodius.mdw.clm.common.publish.io.Options;
import com.sodius.mdw.clm.common.publish.operations.PublishURIConverter;
import com.sodius.mdw.core.model.Model;

/**
 * The main operation publishing the sample content.
 */
class SamplePublishOperation extends PublishOperation<Object> {

    private final File outputFile;

    SamplePublishOperation(File outputFile, IDialogSettings settings, PublishContext context) {
        super("Publishing Sample file", settings, context);
        this.outputFile = outputFile;
    }

    @Override
    protected void run(IProgressMonitor monitor) {
        Model sourceModel = null;
        Model publisherModel = null;
        try {

            // Step 1 - read authoring tool data
            sourceModel = run(new SampleReadModelOperation(getSettings(), getPublishContext()), monitor);
            if (isCanceledOrFailed(monitor)) {
                return;
            }

            // Step 2 - create empty Publisher model
            publisherModel = getPublishContext().getWorkbench().getMetamodelManager().getMetamodel(PublishPackage.eINSTANCE).createModel();
            if (isCanceledOrFailed(monitor)) {
                return;
            }

            // Step 3 - create design
            getPublishContext().getProperties().setProperty(URIConverter.class.getName(), new PublishURIConverter());
            run(new CreateDesign(sourceModel, publisherModel, getPublishContext()), monitor);

            // Step 4 - save Publisher model (requesting to validate consistency before serialization)
            getPublishContext().getProperties().setProperty(Options.OPTION_VALIDATE, true);
            run(new SaveModelOperation(publisherModel, outputFile, getPublishContext().getProperties().getProperties(), getSettings(),
                    getPublishContext()), monitor);

        } catch (CoreException e) {
            getStatus().add(e.getStatus());
        } catch (com.sodius.mdw.core.CoreException e) {
            getStatus().addError(e.getMessage(), e);
        } finally {

            // clear the models to free memory
            if (sourceModel != null) {
                sourceModel.clear();
            }
            if (publisherModel != null) {
                publisherModel.clear();
            }
        }
    }

}
