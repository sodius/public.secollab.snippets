package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlTestScript extends XmlElementWrapper {

    public static Collection<XmlTestScript> getInstances(Model model) {
        Collection<XmlTestScript> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("testScript".equals(element.getTagName())) {
                result.add(new XmlTestScript(element));
            }
        }
        return result;
    }

    XmlTestScript(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public List<XmlElementWrapper> getUsedBy() {
        return getReferencedElements("usedBy");
    }

}
