package com.sodius.mdw.clm.client.tool.sample.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class XmlDiagram extends XmlElementWrapper {

    public static Collection<String> getTypes(Model model) {
        Collection<String> result = new HashSet<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("diagram".equals(element.getTagName())) {
                result.add(element.getAttribute("type"));
            }
        }
        return result;
    }

    public static Collection<XmlDiagram> getInstances(Model model, String type) {
        Collection<XmlDiagram> result = new ArrayList<>();
        for (Element element : model.<Element> getInstances(XmlPackage.Literals.ELEMENT)) {
            if ("diagram".equals(element.getTagName()) && type.equals(element.getAttribute("type"))) {
                result.add(new XmlDiagram(element));
            }
        }
        return result;
    }

    XmlDiagram(Element element) {
        super(element);
    }

    public String getId() {
        return getElement().getAttribute("id");
    }

    public String getName() {
        return getElement().getAttribute("name");
    }

    public String getType() {
        return getElement().getAttribute("type");
    }

    public List<XmlDiagramNode> getNodes() {
        List<XmlDiagramNode> result = new ArrayList<>();
        for (Object content : getElement().getContent()) {
            if (content instanceof Element) {
                Element contentElement = (Element) content;
                if ("node".equals(contentElement.getTagName())) {
                    result.add(new XmlDiagramNode(contentElement));
                }
            }
        }
        return result;
    }

    public List<XmlDiagramEdge> getEdges() {
        List<XmlDiagramEdge> result = new ArrayList<>();
        for (Object content : getElement().getContent()) {
            if (content instanceof Element) {
                Element contentElement = (Element) content;
                if ("edge".equals(contentElement.getTagName())) {
                    result.add(new XmlDiagramEdge(contentElement));
                }
            }
        }
        return result;
    }
}
