package com.sodius.mdw.clm.client.tool.sample.cm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.PublishOperation;
import com.sodius.mdw.clm.client.tool.sample.CreatePropertyView;
import com.sodius.mdw.clm.client.tool.sample.MediaFactory;
import com.sodius.mdw.clm.common.publish.Design;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.PropertyView;
import com.sodius.mdw.clm.common.publish.Type;
import com.sodius.mdw.core.model.Model;

/**
 * Create the containers of Change Requests.
 */
public class CreateChangeRequestContainers extends PublishOperation<Void> {

    private final Model sourceModel;
    private final Design design;

    public CreateChangeRequestContainers(Model sourceModel, Design design, PublishContext context) {
        super("Create Issue Containers", context.getSettings(), context);
        this.sourceModel = sourceModel;
        this.design = design;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // Step 1 - Create a Type for change request container
        Media containerIcon = MediaFactory.getInstance(getPublishContext()).createChangeRequestContainerIcon();
        Type type = run(new CreateChangeRequestContainerType(design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 2 - create a Resource for the change request container
        run(new CreateChangeRequestContainer(type, containerIcon, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 3 - create all change requests
        run(new CreateChangeRequests(sourceModel, design, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }

        // Step 4 - create property views for the Type to display its resources
        PropertyView view = run(new CreatePropertyView(type, getPublishContext()), monitor);
        type.setPropertyView(view);
        type.setPreview(view);

        // Step 5 - create pages
        run(new CreateChangeRequestContainerPage(sourceModel, getPublishContext()), monitor);
        if (isCanceledOrFailed(monitor)) {
            return;
        }
    }
}
