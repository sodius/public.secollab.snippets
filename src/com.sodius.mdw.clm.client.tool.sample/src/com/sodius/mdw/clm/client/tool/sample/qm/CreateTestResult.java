package com.sodius.mdw.clm.client.tool.sample.qm;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sodius.mdw.clm.client.PublishContext;
import com.sodius.mdw.clm.client.tool.sample.AbstractCreateResource;
import com.sodius.mdw.clm.client.tool.sample.Identifiers;
import com.sodius.mdw.clm.client.tool.sample.Mapping;
import com.sodius.mdw.clm.client.tool.sample.model.XmlTestResult;
import com.sodius.mdw.clm.common.model.GUID;
import com.sodius.mdw.clm.common.publish.Media;
import com.sodius.mdw.clm.common.publish.TestResult;
import com.sodius.mdw.clm.common.publish.Type;

/**
 * Create a test result
 */
class CreateTestResult extends AbstractCreateResource<TestResult> {

    private final XmlTestResult testResult;

    CreateTestResult(XmlTestResult testResult, Type type, Media icon, PublishContext context) {
        super("Create Test Result: " + testResult.getId(), type, icon, context);
        this.testResult = testResult;
    }

    @Override
    protected void run(IProgressMonitor monitor) {

        // create a Test Result
        GUID guid = Identifiers.forTestResultResource(testResult);
        TestResult resource = getType().createTestResult(guid, testResult.getName());
        resource.setShortTitle(testResult.getId());
        resource.setIcon(getIcon());
        setResult(resource);

        // trace the mapping between the input XML element and the corresponding created resource
        Mapping.getInstance(getPublishContext()).put(testResult, resource);

        // set attribute values
        String status = testResult.getStatus();
        if (status != null) {
            resource.createValue(getType().getProperty(CreateTestResultType.PROPERTY_STATUS), status);
        }

        // create links
        createLink(resource, CreateTestResultType.PROPERTY_CONTAINER, CreateTestContainer.CONTAINER_INPUT);
        createLinks(resource, CreateTestResultType.PROPERTY_REPORTS_ON, testResult.getReportsOn());
        createLinks(resource, CreateTestResultType.PROPERTY_EXECUTES, testResult.getExecutes());
        createLinks(resource, CreateTestResultType.PROPERTY_RUNS_ON, testResult.getRunsOn());
    }

}
